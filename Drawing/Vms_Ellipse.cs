﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Globalization;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.ComponentModel;

namespace Drawing
{
    [XDrawGeometry("EllipseGeometry")]
    public class Vms_Ellipse : Vms_DrawingShape
    {
        #region DependencyProperties

        public static readonly DependencyProperty RadiusXProperty = DependencyProperty.Register(
       "RadiusX", typeof(double), typeof(Vms_DrawingShape), new PropertyMetadata(1.0, RadiusXEllipseChanged));

        public static readonly DependencyProperty RadiusYProperty = DependencyProperty.Register(
      "RadiusY", typeof(double), typeof(Vms_DrawingShape), new PropertyMetadata(1.0, RadiusYEllipseChanged));

        #endregion

        #region Variables

        private ControlPoint m_MoveControlPoint;
        private static string NameEllipse;
        private static string LabelEllipse = "0,0";

        #endregion

        #region Properties

        public double RadiusX
        {
            get { return (double)GetValue(RadiusXProperty); }
            set { SetValue(RadiusXProperty, value); }
        }

        public double RadiusY
        {
            get { return (double)GetValue(RadiusYProperty); }
            set { SetValue(RadiusYProperty, value); }
        }

        public EllipseGeometry EllipseGeom
        {
            get { return Path.Data as EllipseGeometry; }
        }

        #endregion

        #region Constructor

        public Vms_Ellipse(Path path)
        : base(path)
        {
            var shape = this;
            if (!(Path.Data is EllipseGeometry))
            {
                throw new ArgumentException("path.Data must be of type EllipseGeometry");
            }
            m_MoveControlPoint = new ControlPoint(this, EllipseGeom, EllipseGeometry.CenterProperty, 0, false, false);
            Path.Tag = this;
            RadiusX = EllipseGeom.RadiusX;
            shape.Name = NameEllipse;
            shape.LabelPosition = Point.Parse(LabelEllipse);         
            if(Vms_Drawing.isCircle)
            {
                DependencyPropertyDescriptor.FromProperty(EllipseGeometry.RadiusXProperty, typeof(EllipseGeometry)).AddValueChanged(Path.Data, RadiusXChanged);
            }
        }

        #endregion

        #region Internal Methods

        protected internal override Point GetPoint()
        {
            return m_MoveControlPoint.GetPoint();
        }

        protected override void SetPoint(Point pt)
        {
                m_MoveControlPoint.SetPoint(pt);   
        }

        internal override void CreateControlPoints(bool editMode)
        {
            ControlPoint cp;
            CreateControlPoints(editMode, out cp);
        }

        #endregion

        #region Public Methods
        public void CreateControlPoints(bool editMode, out ControlPoint radiusXcp)
        {
                radiusXcp = new ControlPoint(this, EllipseGeom, EllipseGeometry.RadiusYProperty, 1, true, false);
                radiusXcp.RelativeTo = EllipseGeometry.CenterProperty;
                shape_ControlPoints.Add(radiusXcp);

                radiusXcp = new ControlPoint(this, EllipseGeom, EllipseGeometry.RadiusXProperty, 0, true, false);
                radiusXcp.RelativeTo = EllipseGeometry.CenterProperty;
                shape_ControlPoints.Add(radiusXcp);
        }

        #endregion

        #region Events

        private void RadiusXChanged(object sender, EventArgs e)
        {
            if (RadiusX != EllipseGeom.RadiusX)
            {
                RadiusX = EllipseGeom.RadiusX;
                RadiusY = RadiusX;
            }  
        }

        private static void RadiusXEllipseChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var radiusX = (d as Vms_Ellipse).EllipseGeom.RadiusX;
            if (radiusX != (double)e.NewValue)
            {
                radiusX = (double)e.NewValue;
                (d as Vms_Ellipse).EllipseGeom.RadiusX = radiusX;
            }
        }

        private static void RadiusYEllipseChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var radiusY = (d as Vms_Ellipse).EllipseGeom.RadiusY;
            if (radiusY != (double)e.NewValue)
            {
                radiusY = (double)e.NewValue;
                (d as Vms_Ellipse).EllipseGeom.RadiusY = radiusY;
            }
        }

        #endregion

    }
}
