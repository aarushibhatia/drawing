﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Shapes;

namespace Drawing
{
    public class ControlPoint : IDisposable, IDragableObject
    {

        #region Variables

        private EllipseGeometry m_CPGeom = null;
        private LineGeometry m_ConnectionLine = null;
        private DependencyObject m_Object;
        private DependencyProperty m_Prop;
        private DependencyProperty m_RelativeToProp;
        private bool m_IsSelected = false;
        private bool m_IsSelectable;
        private ControlPoint m_ConnectedTo = null;
        private int m_Dings = 0;
        public static double ZoomValue = 1.0;

        #endregion

        #region Properties

        public ControlPoint SubPoint { get; set; }

        public Vms_DrawingShape Shape { get; private set; }

        public object Tag { get; set; }

        public bool IsSelectable
        {
            get { return m_IsSelectable; }
            set { m_IsSelectable = value; }
        }

        public ControlPoint ConnectedTo
        {
            get { return m_ConnectedTo; }
            set
            {
                m_ConnectedTo = value;
                if (m_ConnectedTo != null)
                {
                    m_ConnectionLine = new LineGeometry(m_ConnectedTo.GetPoint(), GetPoint());
                    Vms_Drawing.ControlLineGroup.Children.Add(m_ConnectionLine);
                    DependencyPropertyDescriptor.FromProperty(m_ConnectedTo.m_Prop, m_ConnectedTo.m_Object.GetType()).AddValueChanged(m_ConnectedTo.m_Object, PropertyChanged);
                }
            }
        }

        public DependencyProperty RelativeTo
        {
            get { return m_RelativeToProp; }
            set
            {
                m_RelativeToProp = value;
                DependencyPropertyDescriptor.FromProperty(m_RelativeToProp, m_Object.GetType()).AddValueChanged(m_Object, RelativeToPropertyChanged);
                m_CPGeom.Center = GetPoint();
            }
        }

        public bool IsSelected
        {
            get { return m_IsSelected; }
            set
            {
                if (m_IsSelected != value)
                {
                    m_IsSelected = value;
                    if (m_CPGeom != null)
                    {
                        if (m_IsSelected)
                        {
                            Vms_Drawing.ControlPointGroup.Children.Remove(m_CPGeom);
                            Vms_Drawing.SelectedControlPointGroup.Children.Add(m_CPGeom);
                        }
                        else
                        {
                            Vms_Drawing.SelectedControlPointGroup.Children.Remove(m_CPGeom);
                            Vms_Drawing.ControlPointGroup.Children.Add(m_CPGeom);
                        }
                    }
                }
            }
        }

        #endregion

        #region Constructor

        public ControlPoint(Vms_DrawingShape shape,
         DependencyObject obj, DependencyProperty prop,
      int dings, bool visible, bool isSelectable)
        {
            ConnectedTo = null;
            Shape = shape;
            m_Object = obj;
            m_Prop = prop;
            m_Dings = dings;
            m_IsSelectable = isSelectable;
            if (m_Prop != null)
            {
                DependencyPropertyDescriptor.FromProperty(m_Prop, m_Object.GetType()).AddValueChanged(m_Object, PropertyChanged);
            }
            if (visible)
            {
                m_CPGeom = new EllipseGeometry(GetPoint(), 5.0 / ZoomValue, 5.0 / ZoomValue);
                Vms_Drawing.ControlPointGroup.Children.Add(m_CPGeom);
            }
        }

        #endregion

        #region Destructor

        ~ControlPoint()
        {
            Dispose(false);
        }

        #endregion

        #region Methods

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (m_CPGeom != null)
                {
                    if (m_IsSelected)
                    {
                        Vms_Drawing.SelectedControlPointGroup.Children.Remove(m_CPGeom);
                    }
                    else
                    {
                        Vms_Drawing.ControlPointGroup.Children.Remove(m_CPGeom);
                    }
                }
                m_CPGeom = null;
                if (m_ConnectionLine != null)
                {
                    Vms_Drawing.ControlLineGroup.Children.Remove(m_ConnectionLine);
                }
                m_ConnectionLine = null;
            }
        }

        internal void ZoomChanged(double zoomFactor)
        {
            if (m_CPGeom != null)
            {
                m_CPGeom.RadiusX = 5.0 / zoomFactor;
                m_CPGeom.RadiusY = 5.0 / zoomFactor;
            }
        }

        /// <summary>
        /// Sets the point "pt" of the shape. Fixes the point on canvas.
        /// </summary>
        /// <param name="pt">Point clicked on canvas.</param>
        public void SetPoint(Point pt)
        {
            Point ptR = new Point();
            if (m_Prop != null)
            {
                if (SubPoint != null)
                {
                    var offset = pt - GetPoint();
                    SubPoint.SetPoint(SubPoint.GetPoint() + offset);
                }
                if (m_Prop.PropertyType == typeof(double))
                {
                    if (m_RelativeToProp != null)
                    {
                        ptR = (Point)m_Object.GetValue(m_RelativeToProp);
                        pt -= (Vector)ptR;
                    }
                    if (m_Dings == 0)
                    {
                        if (pt.X < ptR.X)
                        {
                            ptR.X = pt.X - ptR.X;
                            m_Object.SetValue(m_Prop, pt.X);
                        }                         
                    }
                    else if(m_Dings == 1)
                    {
                        if(pt.Y < ptR.Y)
                        {
                            ptR.Y = pt.Y - ptR.Y;
                            m_Object.SetValue(m_Prop, pt.Y);
                        }                  
                    }
                    else
                    {
                        if (pt.Y < ptR.Y)
                        {
                            ptR.Y = pt.Y - ptR.Y;
                            m_Object.SetValue(m_Prop, pt.Y);
                        }
                        if (pt.X < ptR.X)
                        {
                            ptR.X = pt.X - ptR.X;
                            m_Object.SetValue(m_Prop, pt.X);
                        }
                    }
                }
                else if (m_Prop.PropertyType == typeof(Point))
                {
                    m_Object.SetValue(m_Prop, pt);
                }
                else if (m_Prop.PropertyType == typeof(Rect))
                {
                    Rect rect = (Rect)m_Object.GetValue(m_Prop);
                    switch (m_Dings)
                    {
                        case -1:
                            rect.Location = pt;
                            break;

                        case 0:
                            if (pt.X > rect.Right)
                            {
                                pt.X = rect.Right;
                            }
                            if (pt.Y > rect.Bottom)
                            {
                                pt.Y = rect.Bottom;
                            }
                            rect.Width = rect.Right - pt.X;
                            rect.Height = rect.Bottom - pt.Y;
                            rect.Location = pt;
                            break;

                        case 1:
                            if (pt.X < rect.Left)
                            {
                                pt.X = rect.Left;
                            }
                            if (pt.Y > rect.Bottom)
                            {
                                pt.Y = rect.Bottom;
                            }
                            rect.Width = pt.X - rect.Left;
                            rect.Height = rect.Bottom - pt.Y;
                            rect.Location = new Point(rect.Location.X, pt.Y);
                            break;

                        case 2:
                            if (pt.X > rect.Right)
                            {
                                pt.X = rect.Right;
                            }
                            if (pt.Y < rect.Top)
                            {
                                pt.Y = rect.Top;
                            }
                            rect.Width = rect.Right - pt.X;
                            rect.Location = new Point(pt.X, rect.Location.Y);
                            rect.Height = pt.Y - rect.Top;
                            break;

                        case 3:
                            if (pt.X < rect.Left)
                            {
                                pt.X = rect.Left;
                            }
                            if (pt.Y < rect.Top)
                            {
                                pt.Y = rect.Top;
                            }
                            rect.Width = pt.X - rect.Left;
                            rect.Height = pt.Y - rect.Top;
                            break;
                    }
                    m_Object.SetValue(m_Prop, rect);
                }
            }
        }

        /// <summary>
        /// Returns the vertex of any shape or controlpoint.
        /// </summary>
        /// <returns></returns>
        public Point GetPoint()
        {
            if (m_Prop != null)
            {
                if (m_Prop.PropertyType == typeof(double))
                {
                    Point pt = new Point();
                    if (m_Dings == 0)
                    {
                        pt.X = (double)m_Object.GetValue(m_Prop);
                        pt.Y = 0.0;
                    }
                    else
                    {
                        pt.X = 0.0;
                        pt.Y = (double)m_Object.GetValue(m_Prop);
                    }
                    if (m_RelativeToProp != null)
                    {
                        var ptR = (Point)m_Object.GetValue(m_RelativeToProp);
                        pt += (Vector)ptR;
                    }
                    return pt;
                }
                else if (m_Prop.PropertyType == typeof(Point))
                {
                    return (Point)m_Object.GetValue(m_Prop);
                }
                else if (m_Prop.PropertyType == typeof(Rect))
                {
                    Rect rect = (Rect)m_Object.GetValue(m_Prop);
                    switch (m_Dings)
                    {
                        case -1:
                        case 0: return rect.TopLeft;
                        case 1: return rect.TopRight;
                        case 2: return rect.BottomLeft;
                        case 3: return rect.BottomRight;
                    }
                }
            }
            return new Point(0, 0);
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PropertyChanged(object sender, EventArgs e)
        {
            if (m_CPGeom != null)
            {
                m_CPGeom.Center = GetPoint();
            }
            if (m_ConnectionLine != null)
            {
                m_ConnectionLine.StartPoint = m_ConnectedTo.GetPoint();
                m_ConnectionLine.EndPoint = GetPoint();
            }
        }

        private void RelativeToPropertyChanged(object sender, EventArgs e)
        {
            if (m_CPGeom != null)
            {
                m_CPGeom.Center = GetPoint();
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        #region IDragableObject Members

        private bool m_IsDraged = false;

        public HitTestInfo? HitTest(Point pt)
        {
            var d = pt - GetPoint();
            if (d.X * d.X + d.Y * d.Y <= (5.0 / ZoomValue) * (5.0 / ZoomValue))
            {
                HitTestInfo hti = new HitTestInfo();
                hti.Shape = Shape;
                hti.ControlPoint = this;
                hti.Offset = d;
                return hti;
            }
            return null;
        }

        public void StartDrag()
        {
            m_IsDraged = true;
        }

        public void DragObject(Point pt)
        {
            SetPoint(pt);
        }

        public void EndDrag()
        {
            m_IsDraged = false;
        }

        public bool IsDraged
        {
            get { return m_IsDraged; }
            internal set { m_IsDraged = value; }
        }

        #endregion
    
    }
}
