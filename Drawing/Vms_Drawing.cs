﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Linq;
using System.Globalization;
using System.Collections;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Controls.Primitives;
using Microsoft.VisualBasic;
using System.Timers;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Drawing
{
    #region Enums

    public enum XDrawingModes
    {
        NewPolygon,
        NewLineString,
        NewLine,
        NewRect,
        NewEllipse,
        NewPath,
        NewText,
        EditShapes,
        Name,
        StopLabel
    }

    public enum XDrawingExportFormat
    {
        Canvas,
        DrawingImage
    }

    #endregion

    public class Vms_Drawing : DependencyObject, IDisposable
    {
        #region DependencyProperties

        public static readonly DependencyProperty SelectedShapeProperty = DependencyProperty.Register(
      "SelectedShape", typeof(Vms_DrawingShape), typeof(Vms_Drawing), new PropertyMetadata(null));

        public static readonly DependencyProperty ModeProperty = DependencyProperty.Register(
        "Mode", typeof(XDrawingModes), typeof(Vms_Drawing), new PropertyMetadata(XDrawingModes.Name, ModeChanged));

        public static readonly DependencyProperty IsGridVisibleProperty = DependencyProperty.Register(
        "IsGridVisible", typeof(bool), typeof(Vms_Drawing), new PropertyMetadata(true, IsGridVisibleChanged));

        public static readonly DependencyProperty IsSnapToGridEnabledProperty = DependencyProperty.Register(
        "IsSnapToGridEnabled", typeof(bool), typeof(Vms_Drawing), new PropertyMetadata(true, IsSnapToGridEnabledChanged));

        public static readonly DependencyProperty PointGridModeProperty = DependencyProperty.Register(
        "PointGridMode", typeof(bool), typeof(Vms_Drawing), new PropertyMetadata(false, PointGridModeChanged));

        public static readonly DependencyProperty SnapGridWidthProperty = DependencyProperty.Register(
        "SnapGridWidth", typeof(double), typeof(Vms_Drawing), new PropertyMetadata(1.0, SnapGridWidthChanged));

        public static readonly DependencyProperty DisplayGridWidthProperty = DependencyProperty.Register(
        "DisplayGridWidth", typeof(double), typeof(Vms_Drawing), new PropertyMetadata(10.0, DisplayGridWidthChanged));

        public static readonly DependencyProperty ZoomFactorProperty =
        DependencyProperty.Register("ZoomFactor", typeof(double), typeof(Vms_Drawing), new UIPropertyMetadata(1.0, ZoomFactorChanged));

        public static readonly DependencyProperty SelectedControlPointProperty = DependencyProperty.Register(
        "SelectedControlPoint", typeof(ControlPoint), typeof(Vms_Drawing), new PropertyMetadata(null));

        #endregion

        #region Private Variables

        private DrawingBrush m_GridBrush = null;
        private HitTestInfo? m_DragInfo = null;
        private DrawingBrush m_PointGridBrush = null;
        private ContextMenu m_ContextMenu = null;
        private System.Windows.Shapes.Path m_ControlPointPath;
        private System.Windows.Shapes.Path m_SelectedControlPointPath;
        private static GeometryGroup m_ControlLineGroup;
        private System.Windows.Shapes.Path m_ControlLinePath;
        private bool isSelected = false;
        private Label selectedlabel = null;
        private Vector vector = new Vector();
        private XDrawingModes newMode;
        private Vms_Polygon drawing;
        
        #endregion

        #region Public Variables

        public static GeometryGroup m_ControlPointGroup;
        public static GeometryGroup m_SelectedControlPointGroup;

        #endregion

        #region Lists of Shapes, ControlPoints and Labels

        private static List<Label> m_ShapesNames = new List<Label>();
        internal static List<Vms_DrawingShape> m_Shapes = new List<Vms_DrawingShape>();
        internal static List<ControlPoint> m_ControlPoints = new List<ControlPoint>();

        #endregion

        #region Properties

        public bool isPolygon { get; set; }

        public static bool isCircle { get; set; }

        internal static GeometryGroup SelectedControlPointGroup
        {
            get { return m_SelectedControlPointGroup; }
        }

        public static GeometryGroup ControlPointGroup
        {
            get { return m_ControlPointGroup; }
        }

        public Vms_DrawingShape SelectedShape
        {
            get { return (Vms_DrawingShape)GetValue(SelectedShapeProperty); }
            set
            {
                if (SelectedShape != value)
                {
                    ClearControlPoints();
                    SetValue(SelectedShapeProperty, value);
                    if (value != null)
                    {
                        SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Red);
                        value.CreateControlPoints(true);
                        AddControlPoint(SelectedShape.shape_ControlPoints);
                    }
                }
            }
        }

        public ControlPoint SelectedControlPoint
        {
            get { return (ControlPoint)GetValue(SelectedControlPointProperty); }
            set
            {
                if (SelectedControlPoint != value)
                {
                    if (SelectedControlPoint != null)
                    {
                        SelectedControlPoint.IsSelected = false;
                    }
                    if (value != null)
                    {
                        value.IsSelected = true;
                    }
                    SetValue(SelectedControlPointProperty, value);
                }
            }
        }

        public double ZoomFactor
        {
            get { return (double)GetValue(ZoomFactorProperty); }
            set { SetValue(ZoomFactorProperty, value); }
        }

        public XDrawingModes Mode
        {
            get { return (XDrawingModes)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public bool IsGridVisible
        {
            get { return (bool)GetValue(IsGridVisibleProperty); }
            set { SetValue(IsGridVisibleProperty, value); }
        }

        public bool IsSnapToGridEnabled
        {
            get { return (bool)GetValue(IsSnapToGridEnabledProperty); }
            set { SetValue(IsSnapToGridEnabledProperty, value); }
        }

        public bool PointGridMode
        {
            get { return (bool)GetValue(PointGridModeProperty); }
            set { SetValue(PointGridModeProperty, value); }
        }

        public double SnapGridWidth
        {
            get { return (double)GetValue(SnapGridWidthProperty); }
            set { SetValue(SnapGridWidthProperty, value); }
        }

        public double DisplayGridWidth
        {
            get { return (double)GetValue(DisplayGridWidthProperty); }
            set { SetValue(DisplayGridWidthProperty, value); }
        }

        public static IEnumerable<Vms_DrawingShape> Shapes
        {
            get { return m_Shapes; }
        }

        public static IEnumerable<Label> ShapesNames
        {
            get { return m_ShapesNames; }
        }

        public IEnumerable<Vms_DrawingShape> InverseShapes
        {
            get
            {
                for (int n = m_Shapes.Count - 1; n >= 0; --n)
                {
                    yield return m_Shapes[n];
                }
            }
        }

        public Canvas Canvas { get; private set; }

        internal static GeometryGroup ControlLineGroup
        {
            get { return m_ControlLineGroup; }
        }

        #endregion

        #region Constructor

        public Vms_Drawing(Canvas canvas)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            Canvas = canvas;
            Canvas.LayoutTransform = new ScaleTransform(ZoomFactor, ZoomFactor);

            Canvas.MouseWheel += Canvas_MouseWheel;
            Canvas.MouseDown += Canvas_PreviewMouseDown;
            Canvas.PreviewMouseUp += Canvas_PreviewMouseUp;
            Canvas.PreviewMouseMove += Canvas_PreviewMouseMove;

            m_GridBrush = new DrawingBrush(
            new GeometryDrawing(
            new SolidColorBrush(Colors.White),
            new Pen(new SolidColorBrush(Colors.LightGray), 1.0),
            new RectangleGeometry(new Rect(0, 0, DisplayGridWidth, DisplayGridWidth))));
            m_GridBrush.Stretch = Stretch.None;
            m_GridBrush.TileMode = TileMode.Tile;
            m_GridBrush.Viewport = new Rect(0.0, 0.0, DisplayGridWidth, DisplayGridWidth);
            m_GridBrush.ViewportUnits = BrushMappingMode.Absolute;

            var dg = new DrawingGroup();
            dg.Children.Add(
            new GeometryDrawing(
            new SolidColorBrush(Colors.White),
            new Pen(new SolidColorBrush(Colors.White), 1),
            new RectangleGeometry(new Rect(0, 0, DisplayGridWidth, DisplayGridWidth))));
            dg.Children.Add(
            new GeometryDrawing(
            new SolidColorBrush(Colors.Black),
            null,
            new EllipseGeometry(new Point(0.0, 0.0), 0.6, 0.6)));

            m_PointGridBrush = new DrawingBrush(dg);

            m_PointGridBrush.Stretch = Stretch.None;
            m_PointGridBrush.TileMode = TileMode.Tile;
            m_PointGridBrush.Viewport = new Rect(0.0, 0.0, DisplayGridWidth, DisplayGridWidth);
            m_PointGridBrush.ViewportUnits = BrushMappingMode.Absolute;

            SetDisplayGrid();

            Canvas.Children.Clear();

            m_ControlPointPath = new System.Windows.Shapes.Path();
            m_ControlPointPath.Stroke = new SolidColorBrush(Colors.Blue);
            m_ControlPointPath.StrokeThickness = 1.0;
            m_ControlPointPath.Fill = new SolidColorBrush(Colors.White);
            m_ControlPointPath.Opacity = 0.5;
            m_ControlPointGroup = new GeometryGroup();
            m_ControlPointPath.Data = m_ControlPointGroup;
            Canvas.Children.Add(m_ControlPointPath);

            m_SelectedControlPointPath = new System.Windows.Shapes.Path();
            m_SelectedControlPointPath.Stroke = new SolidColorBrush(Colors.Blue);
            m_SelectedControlPointPath.StrokeThickness = 2.0;
            m_SelectedControlPointPath.Fill = new SolidColorBrush(Colors.White);
            m_SelectedControlPointPath.Opacity = 0.5;
            m_SelectedControlPointGroup = new GeometryGroup();
            m_SelectedControlPointPath.Data = m_SelectedControlPointGroup;
            Canvas.Children.Add(m_SelectedControlPointPath);

            m_ControlLinePath = new System.Windows.Shapes.Path();
            m_ControlLinePath.Stroke = new SolidColorBrush(Colors.Blue);
            m_ControlLinePath.StrokeThickness = 1.0;
            m_ControlLinePath.StrokeDashArray.Add(5.0);
            m_ControlLinePath.StrokeDashArray.Add(5.0);
            m_ControlLinePath.Opacity = 0.5;
            m_ControlLineGroup = new GeometryGroup();
            m_ControlLinePath.Data = m_ControlLineGroup;
            Canvas.Children.Add(m_ControlLinePath);

            Canvas.Focusable = true;

            m_ContextMenu = new ContextMenu();

            ImageBrush brush = new ImageBrush();
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = new Uri(@"C:\Users\i2v\Desktop\GoogleMapTA.jpg");
            src.DecodePixelHeight = 480;
            src.DecodePixelWidth = 640;
            brush.ImageSource = src;
            canvas.Background = brush;
            src.EndInit();

            var mi = new MenuItem();
            var img = new Image();
            mi = new MenuItem();
            mi.Header = "Remove shape";
            mi.Command = ApplicationCommands.Delete;
            mi.CommandTarget = Canvas;
            img = new Image();
            img.Width = 16;
            img.Height = 16;
            img.Source = Canvas.FindResource("DeleteShapeImage") as DrawingImage;
            mi.Icon = img;
            m_ContextMenu.Items.Add(mi);

            Canvas.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, Cut_Executed, Cut_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, Copy_Executed, Copy_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, Paste_Executed, Paste_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, Delete_Executed, Delete_CanExecute));

            Canvas.CommandBindings.Add(new CommandBinding(DrawingCommands.SendToBack, SendToBack_Executed, SendToBack_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(DrawingCommands.OneToBack, OneToBack_Executed, OneToBack_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(DrawingCommands.OneToFront, OneToFront_Executed, OneToFront_CanExecute));
            Canvas.CommandBindings.Add(new CommandBinding(DrawingCommands.SendToFront, SendToFront_Executed, SendToFront_CanExecute));
        }

        #endregion

        #region Destructor

        ~Vms_Drawing()
        {
            Dispose(false);
        }

        #endregion

        #region Private and Protected Methods

        /// <summary>
        /// Contains functionality of mouse click on canvas.
        /// Different modes behave differently on left and right click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Canvas_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Canvas.Focus();
                Keyboard.Focus(Canvas);
                var pt = e.GetPosition(Canvas);
                if (e.ChangedButton == MouseButton.Left)
                {
                    foreach(var shape in m_Shapes)
                    {
                        shape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    }
                    switch (Mode)
                    {
                        case XDrawingModes.StopLabel:
                            if (selectedlabel != null)
                            {
                                var shape2 = m_Shapes.Where(s => s.Name == selectedlabel.Content.ToString()).First();
                                shape2.LabelPosition = selectedlabel.TransformToAncestor(Canvas).Transform(new Point(0, 0));
                                selectedlabel = null;
                            }
                            Mode = XDrawingModes.Name;
                            break;

                        case XDrawingModes.NewPolygon:
                            m_DragInfo = HitTest(pt);
                            var shape = this.SelectedShape as Vms_Polygon;
                            if (shape != null)
                            {
                                if (shape != null && m_DragInfo.Value.ControlPoint != null && m_DragInfo.Value.ControlPoint.Tag != null)
                                {
                                    ArrayList result = AddSegment(m_DragInfo.Value.ControlPoint.Tag, true, false);
                                    this.SelectedControlPoint = (ControlPoint)result[0];
                                    m_DragInfo = HitTest((Point)result[1]);
                                }
                            }
                            if (m_DragInfo.Value.Shape != SelectedShape)
                            {
                                SelectedShape = m_DragInfo.Value.Shape;

                            }
                            if (m_DragInfo.Value.ControlPoint != SelectedControlPoint)
                            {
                                if (m_DragInfo.Value.ControlPoint == null || m_DragInfo.Value.ControlPoint.IsSelectable)
                                {
                                    SelectedControlPoint = m_DragInfo.Value.ControlPoint;
                                }
                            }
                            m_DragInfo.Value.DragObject.StartDrag();
                            break;

                        case XDrawingModes.Name:
                            m_DragInfo = HitTest(pt);
                            if (m_DragInfo != null)
                            {
                                if (m_DragInfo.Value.Shape != SelectedShape)
                                {
                                    SelectedShape = m_DragInfo.Value.Shape;
                                }
                            }
                            if (SelectedShape != null)
                            {
                                SelectedShape.CreateControlPoints(Mode == XDrawingModes.EditShapes);
                            }
                            m_DragInfo = null;
                            Mode = XDrawingModes.StopLabel;
                            break;

                        case XDrawingModes.EditShapes:
                            selectedlabel = null;
                            m_DragInfo = HitTest(pt);
                            if (!m_DragInfo.HasValue)
                            {
                                SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                                SelectedShape = null;
                            }
                            else
                            {
                                if (m_DragInfo.Value.Shape != SelectedShape)
                                {
                                    SelectedShape = m_DragInfo.Value.Shape;
                                }
                                SelectedShape.CreateControlPoints(Mode == XDrawingModes.EditShapes);
                                SelectedShape = m_DragInfo.Value.Shape;
                                vector = SelectedShape.LabelPosition - Mouse.GetPosition(Canvas);
                                var name = m_ShapesNames.Where(s => s.Content.ToString() == SelectedShape.Name).First();

                                isSelected = true;
                                UpdateUILabelPosistion(name);

                                if (m_DragInfo.Value.ControlPoint != SelectedControlPoint)
                                {
                                    isSelected = false;
                                    if (m_DragInfo.Value.ControlPoint == null || m_DragInfo.Value.ControlPoint.IsSelectable)
                                    {
                                        SelectedControlPoint = m_DragInfo.Value.ControlPoint;
                                    }
                                }
                                m_DragInfo.Value.DragObject.StartDrag();
                            }
                            break;

                        case XDrawingModes.NewLine:
                        case XDrawingModes.NewRect:
                        case XDrawingModes.NewEllipse:
                        case XDrawingModes.NewPath:
                        case XDrawingModes.NewText:
                            newMode = Mode;
                            m_DragInfo = CreateNewByDrag(SnapToGrid(pt), out newMode);
                            if (m_DragInfo.HasValue)
                            {
                                Mode = newMode;
                                SetValue(SelectedShapeProperty, m_DragInfo.Value.Shape);
                                m_Shapes.Add(m_DragInfo.Value.Shape);
                                Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), m_DragInfo.Value.Shape.Path);
                                m_DragInfo.Value.DragObject.StartDrag();
                                AddControlPoint(SelectedShape.shape_ControlPoints);
                            }
                            break;
                    }
                }
                else if (e.ChangedButton == MouseButton.Right)
                {
                    pt = e.GetPosition(Canvas);
                    pt.X += 10;
                    pt.Y += 10;
                    var shape = this.SelectedShape;
                    switch (Mode)
                    {
                        case XDrawingModes.Name:
                            isSelected = false;
                            SelectedShape.LabelPosition = pt;
                            if (m_DragInfo != null)
                            {
                                m_DragInfo.Value.DragObject.EndDrag();
                                m_DragInfo = null;
                            }
                            SelectedShape.Name = Interaction.InputBox("Enter the name of shape!", "", "");
                            Label label = new Label();
                            label.Height = 50;
                            label.Width = 100;
                            label.FontSize = 18;
                            label.FontWeight = FontWeights.Bold;
                            label.Content = SelectedShape.Name;
                            label.Margin = new Thickness(pt.X, pt.Y, 0, 0);
                            label.MouseDown += Label_MouseDown;
                            Canvas.Children.Add(label);
                            m_ShapesNames.Add(label);
                            SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                            SelectedShape = null;

                            break;

                        case XDrawingModes.NewPolygon:
                            shape = this.SelectedShape as Vms_Polygon;
                            if (shape != null)
                            {
                                if (shape != null && m_DragInfo.Value.ControlPoint != null && m_DragInfo.Value.ControlPoint.Tag != null)
                                {
                                    if (m_DragInfo.Value.ControlPoint.Tag is PathFigure)
                                    {
                                        if(isPolygon)
                                        {
                                            var figure = m_DragInfo.Value.ControlPoint.Tag as PathFigure;
                                            figure.IsClosed = true;
                                            this.SelectedControlPoint = null;
                                            Mode = XDrawingModes.Name;
                                        }
                                        else
                                        {
                                            var figure = m_DragInfo.Value.ControlPoint.Tag as PathFigure;
                                            figure.IsClosed = false;
                                            this.SelectedControlPoint = null;
                                            Mode = XDrawingModes.Name;
                                        }
                                        
                                    }
                                }
                            }
                            m_DragInfo.Value.DragObject.EndDrag();
                            m_DragInfo = null;
                            SelectedShape.LabelPosition = pt;
                            SelectedShape.Name = Interaction.InputBox("Enter the name of shape!", "", "");
                            Label label2 = new Label();
                            label2.Height = 50;
                            label2.Width = 100;
                            label2.FontSize = 18;
                            label2.FontWeight = FontWeights.Bold;
                            label2.Content = SelectedShape.Name;
                            label2.Margin = new Thickness(pt.X, pt.Y, 0, 0);
                            label2.MouseDown += Label_MouseDown;
                            Canvas.Children.Add(label2);
                            m_ShapesNames.Add(label2);
                            SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                            SelectedShape = null;
                            //  ClearControlPoints();
                            break;

                        case XDrawingModes.EditShapes:
                            if (m_DragInfo != null)
                            {
                                m_DragInfo.Value.DragObject.EndDrag();
                                m_DragInfo = null;
                            }
                            foreach (var name in m_ShapesNames)
                            {
                                if (name.Content.ToString() == SelectedShape.Name)
                                {
                                    SelectedShape.LabelPosition = name.TransformToAncestor(Canvas).Transform(new Point(0, 0));
                                }
                            }
                            isSelected = false;
                            //SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                            break;

                        case XDrawingModes.StopLabel:
                            isSelected = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Adds control points of shapes to list of canvas's control points
        /// </summary>
        /// <param name="cp_List"></param>
        internal void AddControlPoint(List<ControlPoint> cp_List)
        {
            foreach (var cp in cp_List)
            {
                if (m_ControlPoints.Contains(cp))
                {
                    break;
                }
                else
                {
                    m_ControlPoints.Add(cp);
                }

            }
        }

        internal void ClearControlPoints()
        {
            if (SelectedShape != null)
            {
                for (int n = SelectedShape.shape_ControlPoints.Count - 1; n >= 0; --n)
                {
                    SelectedShape.shape_ControlPoints[n].Dispose();
                }
                SelectedShape.shape_ControlPoints.Clear();
            }
            else
            {
                for (int n = m_ControlPoints.Count - 1; n >= 0; --n)
                {
                    m_ControlPoints[n].Dispose();
                }
                m_ControlPoints.Clear();
            }

        }

        /// <summary>
        /// Adds line segment while creating a polygon
        /// </summary>
        /// <param name="relativeTo"></param>
        /// <param name="after"></param>
        /// <param name="Is_Closed"></param>
        /// <returns></returns>
        internal ArrayList AddSegment(object relativeTo, bool after, bool Is_Closed)
        {
            var result = new ArrayList();
            Point? newpoint = null;
            Point? startPoint = null;
            var newSegment = new LineSegment();
            var lastLine = new LineSegment();
            if (relativeTo is PathFigure)
            {
                var figure = relativeTo as PathFigure;
                if (Is_Closed)
                {
                    figure.IsClosed = true;
                }
                else if (after)
                {
                    newSegment.Point = figure.StartPoint;
                    newpoint = newSegment.Point;
                    figure.Segments.Insert(0, newSegment);
                }
                else
                {
                    var ept = GetSegmentEndPoint(figure, figure.Segments[figure.Segments.Count - 1]);
                    newSegment.Point = ept + (figure.StartPoint - ept) * 0.5;
                    newpoint = newSegment.Point;
                    figure.Segments.Add(newSegment);
                }
            }
            else if (relativeTo is PathSegment)
            {
                var segment = relativeTo as PathSegment;
                PathFigure figure = null;
                foreach (var f in drawing.PathGeom.Figures)
                {
                    if (f.Segments.Contains(segment))
                    {
                        figure = f;
                    }
                }
                if (after)
                {
                    Point p1 = GetSegmentEndPoint(figure, segment);

                    Point p2;
                    if (figure.Segments.IndexOf(segment) == figure.Segments.Count - 1)
                    {
                        p2 = figure.StartPoint;
                        lastLine.Point = figure.StartPoint;
                        startPoint = lastLine.Point;
                    }
                    else
                    {
                        p2 = GetSegmentEndPoint(figure, figure.Segments[figure.Segments.IndexOf(segment) + 1]);
                        lastLine.Point = figure.StartPoint;
                        startPoint = lastLine.Point;
                    }
                    newSegment.Point = GetSegmentStartPoint(figure, segment);
                    newpoint = newSegment.Point;
                    figure.Segments.Insert(figure.Segments.IndexOf(segment) + 1, newSegment);
                }
                else
                {
                    var p1 = GetSegmentStartPoint(figure, segment);
                    var p2 = GetSegmentEndPoint(figure, segment);
                    newSegment.Point = p1 + (p2 - p1) * 0.5;
                    newpoint = newSegment.Point;
                    figure.Segments.Insert(figure.Segments.IndexOf(segment), newSegment);
                }
            }
            ClearControlPoints();
            ControlPoint cpDummy;
            result.Add(drawing.CreateControlPoints(true, out cpDummy, newSegment));
            result.Add(newpoint);
            AddControlPoint(SelectedShape.shape_ControlPoints);
            return result;
        }

        /// <summary>
        /// Gets line segment's start point
        /// </summary>
        /// <param name="figure"></param>
        /// <param name="pathSegment"></param>
        /// <returns></returns>
        private Point GetSegmentStartPoint(PathFigure figure, PathSegment pathSegment)
        {
            var n = figure.Segments.IndexOf(pathSegment) - 1;
            if (n < 0)
            {
                return figure.StartPoint;
            }
            return GetSegmentEndPoint(figure, figure.Segments[n]);
        }

        /// <summary>
        /// Gets line segment's end point
        /// </summary>
        /// <param name="figure"></param>
        /// <param name="pathSegment"></param>
        /// <returns></returns>
        public Point GetSegmentEndPoint(PathFigure figure, PathSegment pathSegment)
        {
            if (pathSegment is LineSegment)
            {
                return (pathSegment as LineSegment).Point;
            }
            else if (pathSegment is ArcSegment)
            {
                return (pathSegment as ArcSegment).Point;
            }
            else if (pathSegment is BezierSegment)
            {
                return (pathSegment as BezierSegment).Point3;
            }
            else if (pathSegment is QuadraticBezierSegment)
            {
                return (pathSegment as QuadraticBezierSegment).Point2;
            }
            return new Point();
        }

        internal ControlPoint ChangeSegmentType(ControlPoint controlPoint, string type)
        {
            PathFigure figure = null;
            foreach (var f in drawing.PathGeom.Figures)
            {
                if (f.Segments.Contains(controlPoint.Tag as PathSegment))
                {
                    figure = f;
                }
            }
            if (figure == null)
            {
                throw new ArgumentException("Segment of controlPoint is not in this path");
            }

            Point startPoint = GetSegmentStartPoint(figure, controlPoint.Tag as PathSegment);
            Point endPoint = GetSegmentEndPoint(figure, controlPoint.Tag as PathSegment);
            Point pathControlPoint1 = new Point();
            Point pathControlPoint2 = new Point();

            if (controlPoint.Tag is LineSegment || controlPoint.Tag is ArcSegment)
            {
                pathControlPoint1 = startPoint + (endPoint - startPoint) * 0.3;
                pathControlPoint2 = endPoint + (startPoint - endPoint) * 0.3;
            }
            else if (controlPoint.Tag is BezierSegment)
            {
                pathControlPoint1 = (controlPoint.Tag as BezierSegment).Point1;
                pathControlPoint2 = (controlPoint.Tag as BezierSegment).Point2;
            }
            else if (controlPoint.Tag is QuadraticBezierSegment)
            {
                pathControlPoint1 = startPoint + (endPoint - startPoint) * 0.3;
                pathControlPoint2 = (controlPoint.Tag as QuadraticBezierSegment).Point1;
            }

            PathSegment newSegment = null;
            if (String.CompareOrdinal(type, "LineSegment") == 0)
            {
                var lSegment = new LineSegment();
                lSegment.Point = endPoint;
                newSegment = lSegment;
            }
            else if (String.CompareOrdinal(type, "ArcSegment") == 0)
            {
                var aSegment = new ArcSegment();
                aSegment.Point = endPoint;
                var d = endPoint - startPoint;
                aSegment.Size = new Size(Math.Abs(d.X) * 2.0, Math.Abs(d.Y) * 2.0);
                aSegment.IsLargeArc = false;
                aSegment.SweepDirection = SweepDirection.Clockwise;
                newSegment = aSegment;
            }
            else if (String.CompareOrdinal(type, "BezierSegment") == 0)
            {
                var bSegment = new BezierSegment();
                bSegment.Point1 = pathControlPoint1;
                bSegment.Point2 = pathControlPoint2;
                bSegment.Point3 = endPoint;
                newSegment = bSegment;
            }
            else if (String.CompareOrdinal(type, "QuadraticBezierSegment") == 0)
            {
                var qSegment = new QuadraticBezierSegment();
                qSegment.Point1 = pathControlPoint2;
                qSegment.Point2 = endPoint;
                newSegment = qSegment;
            }

            if (newSegment != null)
            {
                figure.Segments.Insert(figure.Segments.IndexOf(controlPoint.Tag as PathSegment), newSegment);
                figure.Segments.Remove(controlPoint.Tag as PathSegment);
                ClearControlPoints();
                ControlPoint cpDummy;
                controlPoint = drawing.CreateControlPoints(true, out cpDummy, newSegment);
            }
            return controlPoint;
        }

        /// <summary>
        /// Draws out a shape on canvas according to Shape mode.
        /// </summary>
        /// <param name="startPt">Start point of shape on canvas</param>
        /// <param name="newMode">Mode in which shape is to be created</param>
        /// <returns></returns>
        public HitTestInfo CreateNewByDrag(Point startPt, out XDrawingModes newMode)
        {
            newMode = XDrawingModes.Name;
            var hti = new HitTestInfo();
            hti.Offset = new Vector(0.0, 0.0);
            var path = new System.Windows.Shapes.Path();
            path.Stroke = new SolidColorBrush(Colors.Black);
            path.StrokeThickness = 1.0;
            ControlPoint cp;
            switch (Mode)
            {
                case XDrawingModes.NewLine:
                    path.Data = new LineGeometry(startPt, startPt);
                    var line = new Vms_Line(path);
                    line.CreateControlPoints(false, out cp);
                    newMode = XDrawingModes.Name;
                    hti.Shape = line;
                    hti.ControlPoint = cp;
                    hti.Shape.ShapeType = "Vms_Line";
                    break;

                case XDrawingModes.NewRect:
                    path.Data = new RectangleGeometry(new Rect(startPt, new Size(0.0, 0.0)));
                    var rect = new Vms_Rectangle(path);
                    rect.CreateControlPoints(false, out cp);
                    newMode = XDrawingModes.Name;
                    hti.Shape = rect;
                    hti.ControlPoint = cp;
                    hti.Shape.ShapeType = "Vms_Rectangle";
                    break;

                case XDrawingModes.NewEllipse:
                    path.Data = new EllipseGeometry(startPt, 15.0, 15.0);
                    var ellipse = new Vms_Ellipse(path);                  
                    ellipse.CreateControlPoints(false, out cp);
                    newMode = XDrawingModes.Name;
                    hti.Shape = ellipse;
                    hti.ControlPoint = cp;
                    hti.Shape.ShapeType = "Vms_Ellipse";
                    break;

                case XDrawingModes.NewPath:
                    var pathG = new PathGeometry();
                    var figure = new PathFigure();
                    figure.StartPoint = startPt;
                    var segment = new LineSegment(startPt, true);
                    figure.Segments.Add(segment);
                    pathG.Figures.Add(figure);
                    path.Data = pathG;
                    drawing = new Vms_Polygon(path);
                    drawing.CreateControlPoints(true, out cp, null);
                    newMode = XDrawingModes.NewPolygon;
                    hti.Shape = drawing;
                    hti.ControlPoint = cp;
                    hti.Shape.ShapeType = "Vms_Polygon";
                    break;

            }
            return hti;
        }

        
        /// <summary>
        /// Returns information about where mouse is clicked on canvas
        /// </summary>
        /// <param name="pt">Point where mouse is clicked</param>
        /// <returns></returns>
        private HitTestInfo? HitTest(Point pt)
        {
            HitTestInfo? hti = null;
            foreach (var cp in m_ControlPoints)
            {
                hti = cp.HitTest(pt);
                if (hti.HasValue)
                {
                    return hti;
                }
            }
            if (SelectedShape != null)
            {
                hti = SelectedShape.HitTest(pt);
                if (hti.HasValue)
                {
                    return hti;
                }
            }
            foreach (var p in InverseShapes)
            {
                hti = p.HitTest(pt);
                if (hti.HasValue)
                {
                    return hti;
                }
            }
            return null;
        }

        private Point SnapToGrid(Point pt)
        {
            if (IsSnapToGridEnabled && (Keyboard.Modifiers & ModifierKeys.Control) == 0)
            {
                pt.X = Math.Round(pt.X / SnapGridWidth) * SnapGridWidth;
                pt.Y = Math.Round(pt.Y / SnapGridWidth) * SnapGridWidth;
            }
            return pt;
        }

        /// <summary>
        /// Sets label to move the same as mouse pointer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(Mode != XDrawingModes.NewPolygon || Mode != XDrawingModes.NewPath)
            {
                selectedlabel = sender as Label;
                isSelected = true;
            }    
        }

        /// <summary>
        /// Updates shape's label while shape is moved.
        /// </summary>
        /// <param name="element">Label corresponding to the shape</param>
        void UpdateUILabelPosistion(Control element)
        {
            var label = element as Label;
            var shape2 = m_Shapes.Where(s => s.Name == label.Content.ToString()).First();
            Task t1 = new Task(() =>
            {
                while (isSelected)
                {
                    label.Dispatcher.Invoke(new Action(delegate ()
        {
            label.Margin = new Thickness(Mouse.GetPosition(Canvas).X + vector.X, Mouse.GetPosition(Canvas).Y + vector.Y, 0, 0);
        }));
                }
            });
            t1.Start();
        }

        void Canvas_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (m_DragInfo.HasValue)
                {

                }
                Canvas.ReleaseMouseCapture();
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                if(Mode != XDrawingModes.EditShapes && SelectedShape != null)
                {
                    m_ContextMenu.IsOpen = true;
                }
               
            }
            e.Handled = true;
        }

        void Canvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (isSelected && selectedlabel != null)
            {
                selectedlabel.Margin = new Thickness(Mouse.GetPosition(Canvas).X, Mouse.GetPosition(Canvas).Y, 0, 0);
            }
            var pt = e.GetPosition(Canvas);
            if (m_DragInfo != null)
            {
                m_DragInfo.Value.DragObject.DragObject(SnapToGrid(pt - m_DragInfo.Value.Offset));
            }
            e.Handled = true;
        }

        private static void ZoomFactorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;
            drawing.OnZoomFactorChanged(e);
        }

        private void OnZoomFactorChanged(DependencyPropertyChangedEventArgs e)
        {
            Canvas.LayoutTransform = new ScaleTransform((double)e.NewValue, (double)e.NewValue);
            (m_GridBrush.Drawing as GeometryDrawing).Pen.Thickness = 1.0 / (double)e.NewValue;
            (((m_PointGridBrush.Drawing as DrawingGroup).Children[1] as GeometryDrawing).Geometry as EllipseGeometry).RadiusX = 0.6 / (double)e.NewValue;
            (((m_PointGridBrush.Drawing as DrawingGroup).Children[1] as GeometryDrawing).Geometry as EllipseGeometry).RadiusY = 0.6 / (double)e.NewValue;

            m_ControlPointPath.StrokeThickness = 1.0 / (double)e.NewValue;
            m_SelectedControlPointPath.StrokeThickness = 2.0 / (double)e.NewValue;
            m_ControlLinePath.StrokeThickness = 1.0 / (double)e.NewValue;

            foreach (var shape in m_Shapes)
            {
                foreach (var cp in m_ControlPoints)
                {
                    cp.ZoomChanged((double)e.NewValue);
                }
            }
        }

        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            int Delta = e.Delta;
            if (Delta > 0)
            {
                ZoomFactor *= 1.25;
                ControlPoint.ZoomValue = ZoomFactor;
            }
            else
            {
                ZoomFactor /= 1.25;
                ControlPoint.ZoomValue = ZoomFactor;
            }
        }

        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var shape = SelectedShape;
                SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                SelectedShape = null;
                var xDoc = new XDocument(new XElement("x"));
                shape.Export(xDoc.Root);
                Clipboard.SetText(xDoc.Root.FirstNode.ToString());
                RemoveShape(shape);
            }
        }

        private void Cut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var xDoc = new XDocument(new XElement("x"));
                SelectedShape.Export(xDoc.Root);
                Clipboard.SetText(xDoc.Root.FirstNode.ToString());
            }
        }

        private void Copy_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Clipboard.ContainsText() && Clipboard.GetText().StartsWith("<GeometryDrawing"))
            {
                try
                {
                    var xaml = Clipboard.GetText();
                    var xDoc = XDocument.Parse(xaml);
                    Vms_DrawingShape shape = Vms_DrawingShape.LoadFromGeometryDrawing(this, xDoc.Root);
                    if (shape != null)
                    {
                        m_Shapes.Add(shape);
                        Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), shape.Path);
                        SelectedShape = shape;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception while pasting");
                }
            }
        }

        private void Paste_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var fmt = DataFormats.GetDataFormat("XDrawShape");
            e.CanExecute = Clipboard.ContainsText() && Clipboard.GetText().StartsWith("<GeometryDrawing");
            e.Handled = true;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                RemoveShape(SelectedShape);
            }
        }

        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void SendToBack_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var n = m_Shapes.IndexOf(SelectedShape);
                if (n > 0)
                {
                    m_Shapes.RemoveAt(n);
                    m_Shapes.Insert(0, SelectedShape);
                    n = Canvas.Children.IndexOf(SelectedShape.Path);
                    Canvas.Children.RemoveAt(n);
                    Canvas.Children.Insert(0, SelectedShape.Path);
                }
            }
        }

        private void SendToBack_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void OneToBack_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var n = m_Shapes.IndexOf(SelectedShape);
                if (n > 0)
                {
                    m_Shapes.RemoveAt(n);
                    m_Shapes.Insert(n - 1, SelectedShape);

                    n = Canvas.Children.IndexOf(SelectedShape.Path);
                    Canvas.Children.RemoveAt(n);
                    Canvas.Children.Insert(n - 1, SelectedShape.Path);
                }
            }
        }

        private void OneToBack_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void OneToFront_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var n = m_Shapes.IndexOf(SelectedShape);
                if (n < m_Shapes.Count - 1)
                {
                    m_Shapes.RemoveAt(n);
                    m_Shapes.Insert(n + 1, SelectedShape);

                    n = Canvas.Children.IndexOf(SelectedShape.Path);
                    Canvas.Children.RemoveAt(n);
                    Canvas.Children.Insert(n + 1, SelectedShape.Path);
                }
            }
        }

        private void OneToFront_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private void SendToFront_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (SelectedShape != null)
            {
                var n = m_Shapes.IndexOf(SelectedShape);
                if (n < m_Shapes.Count - 1)
                {
                    m_Shapes.RemoveAt(n);
                    m_Shapes.Add(SelectedShape);

                    n = Canvas.Children.IndexOf(SelectedShape.Path);
                    Canvas.Children.RemoveAt(n);
                    Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), SelectedShape.Path);
                }
            }
        }

        private void SendToFront_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedShape != null;
            e.Handled = true;
        }

        private static void ModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;
            if (drawing != null && drawing.SelectedShape != null && (XDrawingModes)e.NewValue != (XDrawingModes)e.OldValue)
            { 
                drawing.SelectedShape.CreateControlPoints(false);
            }
        }

        private void SetDisplayGrid()
        {
            if (IsGridVisible)
            {
                if (PointGridMode)
                {
                    Canvas.Background = m_PointGridBrush;
                }
                else
                {
                    Canvas.Background = m_GridBrush;
                }
            }
            else
            {
                Canvas.Background = new SolidColorBrush(Colors.White);
            }
        }

        private static void IsGridVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;

            drawing.SetDisplayGrid();
        }

        private static void IsSnapToGridEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;

        }

        private static void PointGridModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;
            drawing.SetDisplayGrid();
        }

        private static void SnapGridWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;
        }

        private static void DisplayGridWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var drawing = d as Vms_Drawing;
            drawing.OnDisplayGridWidthChanged(d, e);
        }

        protected virtual void OnDisplayGridWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var width = (double)e.NewValue;
            ((m_GridBrush.Drawing as GeometryDrawing).Geometry as RectangleGeometry).Rect = new Rect(0, 0, width, width);
            m_GridBrush.Viewport = new Rect(0.0, 0.0, width, width);

            (((m_PointGridBrush.Drawing as DrawingGroup).Children[0] as GeometryDrawing).Geometry as RectangleGeometry).Rect = new Rect(0.0, 0.0, width, width);
            m_PointGridBrush.Viewport = new Rect(0.0, 0.0, width, width);
        }

        #endregion

        #region Public Methods

        public XElement Export(XDrawingExportFormat format)
        {
            XElement xDrawing;
            XElement xParent;
            switch (format)
            {
                case XDrawingExportFormat.Canvas:
                    xDrawing = new XElement("Canvas");
                    xParent = xDrawing;
                    break;

                case XDrawingExportFormat.DrawingImage:
                    xParent = new XElement("DrawingGroup");

                    xDrawing = new XElement("DrawingImage",
                        new XElement("DrawingImage.Drawing",
                            xParent));
                    break;

                default:
                    throw new ArgumentException("format not supported");
            }
            foreach (var shape in m_Shapes)
            {
                shape.Export(xParent);
            }
            return xDrawing;
        }

        public void ZoomIn()
        {
            ZoomFactor *= 1.25;
            ControlPoint.ZoomValue = ZoomFactor;
        }

        public void ZoomOut()
        {
            ZoomFactor /= 1.25;
            ControlPoint.ZoomValue = ZoomFactor;
        }

        public void Zoom1()
        {
            ZoomFactor = 1.0;
            ControlPoint.ZoomValue = ZoomFactor;
        }

        public bool Import(DrawProject m_Project)
        {
            return Import(true, m_Project);
        }

        /// <summary>
        /// Imports a drawing from xml.
        /// </summary>
        /// <param name="xElement">Xml element too start searching</param>
        /// <param name="clearDrawing">remove all shapes before import</param>
        /// <returns>true if all xml element could be loaded, false if unknown (skiped) elements have been found</returns>
        /// 
        public bool Import(bool clearDrawing, DrawProject m_Project)
        {
            if (clearDrawing)
            {
                ClearDrawing();
            }
            var lines = File.ReadLines(m_Project.TextFilePath);
            foreach (var line in lines)
            {
                if (line != null)
                {
                    var shape = JsonConvert.DeserializeObject<ShapeProperties>(line);
                    var typeOfShape = shape.Type;
                    Vms_DrawingShape shape2;
                    var path = new System.Windows.Shapes.Path();
                    path.Stroke = new SolidColorBrush(Colors.Black);
                    path.StrokeThickness = 1.0;
                    switch (typeOfShape)
                    {
                        case "Vms_Ellipse":
                            var ellipse = JsonConvert.DeserializeObject<Ellipse>(line);
                            path.Data = new EllipseGeometry(ellipse.Center, ellipse.RadiusX, ellipse.RadiusY);
                            Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), path);
                            shape2 = new Vms_Ellipse(path);
                            shape2.Name = ellipse.Name;
                            shape2.LabelPosition = ellipse.LabelPosition;
                            m_Shapes.Add(shape2);
                            break;

                        case "Vms_Line":
                            var line2 = JsonConvert.DeserializeObject<Line>(line);
                            path.Data = new LineGeometry(line2.StartPoint, line2.EndPoint);
                            Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), path);
                            shape2 = new Vms_Line(path);
                            shape2.Name = line2.Name;
                            shape2.LabelPosition = line2.LabelPosition;
                            m_Shapes.Add(shape2);
                            break;

                        case "Vms_Rectangle":
                            var rect = JsonConvert.DeserializeObject<Rectangle>(line);
                            path.Data = new RectangleGeometry(rect.Rect, rect.RadiusX, rect.RadiusY);
                            Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), path);
                            shape2 = new Vms_Rectangle(path);
                            shape2.Name = rect.Name;
                            shape2.LabelPosition = rect.LabelPosition;
                            m_Shapes.Add(shape2);
                            break;

                        case "Vms_Polygon":
                            var polygon = JsonConvert.DeserializeObject<Polygon>(line);
                            path.Data = polygon.pathGeo as Geometry;
                            Canvas.Children.Insert(Canvas.Children.IndexOf(m_ControlPointPath), path);
                            shape2 = new Vms_Polygon(path);
                            shape2.Name = polygon.Name;
                            shape2.LabelPosition = polygon.LabelPosition;
                            m_Shapes.Add(shape2);
                            break;
                    }
                    Label label = new Label();
                    label.Height = 50;
                    label.Width = 100;
                    label.FontSize = 18;
                    label.FontWeight = FontWeights.Bold;
                    label.Content = shape.Name.ToString();
                    label.Margin = new Thickness(shape.LabelPosition.X, shape.LabelPosition.Y, 0, 0);
                    label.MouseDown += Label_MouseDown;
                    m_ShapesNames.Add(label);
                    Canvas.Children.Add(label);
                }
            }
            return true;
        }

        public void RemoveShape(Vms_DrawingShape shape)
        {   
            if (SelectedShape == shape)
            {
                SelectedControlPoint = null;
                var name = m_ShapesNames.Where(s => s.Content.ToString() == SelectedShape.Name).First();
                Canvas.Children.Remove(name);
                SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                SelectedShape = null;
            }          
            shape.Dispose();
            m_Shapes.Remove(shape);
        }

        public void ClearDrawing()
        {
            SelectedControlPoint = null;
            if(SelectedShape != null)
            {
                SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                SelectedShape = null;
            }
            foreach (var shape in Shapes)
            {
                shape.Dispose();
            }
            m_Shapes.Clear();
        }

        #endregion

        #region IDisposable Members    

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ClearDrawing();
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

    }

    #region IDraggable Members

    public interface IDragableObject
    {
        HitTestInfo? HitTest(Point pt);
        void StartDrag();
        void DragObject(Point pt);
        void EndDrag();
        bool IsDraged { get; }
    }

    public struct HitTestInfo
    {
        public Vms_DrawingShape Shape;
        public ControlPoint ControlPoint;
        public Vector Offset;

        public IDragableObject DragObject
        {
            get { return (ControlPoint as IDragableObject) ?? (Shape as IDragableObject); }
        }
    }

    #endregion

}

