﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Drawing
{
   public static class DrawingCommands
   {
      public static RoutedCommand SendToBack = new RoutedCommand("SendToBack", typeof(DrawingCommands));
      public static RoutedCommand OneToBack = new RoutedCommand("OneToBack", typeof(DrawingCommands));
      public static RoutedCommand OneToFront = new RoutedCommand("OneToFront", typeof(DrawingCommands));
      public static RoutedCommand SendToFront = new RoutedCommand("SendToFront", typeof(DrawingCommands));
   }
}
