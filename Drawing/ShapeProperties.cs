﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Drawing
{
    class ShapeProperties
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public Point LabelPosition { get; set; }

    }

    class Ellipse : ShapeProperties
    {
        public double RadiusX { get; set; }

        public double RadiusY { get; set; }

        public Point Center { get; set; }
    }

    class Line : ShapeProperties
    {
        public Point StartPoint { get; set; }

        public Point EndPoint { get; set; }
    }

    class Rectangle : ShapeProperties
    {
        public double RadiusX { get; set; }

        public double RadiusY { get; set; }

        public Rect Rect { get; set; }
    }

    class Polygon : ShapeProperties
    {
        public Geometry pathGeo { get; set; }
    }
}

