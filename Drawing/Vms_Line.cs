﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Globalization;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Drawing
{
   [XDrawGeometry("LineGeometry")]
   public class Vms_Line : Vms_DrawingShape
   {
        #region Variables

        private static string NameLine;
        private static string LabelLine = "0,0";

        #endregion

        #region Properties

        public LineGeometry LineGeom
        {
            get { return Path.Data as LineGeometry; }
        }

        #endregion

        #region Constructor

        public Vms_Line(Path path)
   : base(path)
        {
            var shape = this;
            if (!(Path.Data is LineGeometry))
            {
                throw new ArgumentException("path.Data must be of type LineGeometry");
            }
            shape.Name = NameLine;
            shape.LabelPosition = Point.Parse(LabelLine);
            Path.Tag = this;
        }

        #endregion

        #region Internal Methods

        protected internal override Point GetPoint()
        {
            return LineGeom.StartPoint;
        }

        protected override void SetPoint(Point pt)
        {
            var d = pt - LineGeom.StartPoint;
            LineGeom.StartPoint = pt;
            LineGeom.EndPoint += d;
        }

        internal override void CreateControlPoints(bool editMode)
        {
            ControlPoint cp;
            CreateControlPoints(editMode, out cp);
        }

        #endregion

        #region Public Methods

        public void CreateControlPoints(bool editMode, out ControlPoint endCP)
        {
            shape_ControlPoints.Add(new ControlPoint(this, LineGeom, LineGeometry.StartPointProperty, 0, true, false));
            endCP = new ControlPoint(this, LineGeom, LineGeometry.EndPointProperty, 0, true, false);
            shape_ControlPoints.Add(endCP);
        }

        #endregion

    }
}
