﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Globalization;
using System.Collections;
using Newtonsoft.Json;

namespace Drawing
{
   [XDrawGeometry("PathGeometry")]
   public class Vms_Polygon : Vms_DrawingShape
   {
        #region Variables

        private static bool m_UseGeometryMiniLanguage = true;
        private static string NamePolygon;
        private static string LabelPolygon = "0,0";

        #endregion

        #region Properties

        public PathGeometry PathGeom
        {
            get { return Path.Data as PathGeometry; }
        }

        #endregion

        #region Constructor

        public Vms_Polygon(Path path)
         : base(path)
        {
            var shape = this;
            if (!(Path.Data is PathGeometry || Path.Data is Geometry))
            {
                throw new ArgumentException("path.Data must be of type PathGeometry");
            }
            shape.Name = NamePolygon;
            shape.LabelPosition = Point.Parse(LabelPolygon);
            Path.Tag = this;
        }

        #endregion

        #region Internal Methods

        protected internal override Point GetPoint()
        {
            return PathGeom.GetFirstStartPoint();
        }

        protected override void SetPoint(Point pt)
        {
            var offset = pt - PathGeom.GetFirstStartPoint();
            PathGeom.DoForAllPoints(new RefPointCallback((ref Point _pt) =>
            {
                _pt += offset;
            }));
        }

        internal override void CreateControlPoints(bool editMode)
        {
            ControlPoint cp;
            CreateControlPoints(editMode, out cp, null);
        }

        #endregion

        #region Public Methods

        public ControlPoint CreateControlPoints(bool editMode, out ControlPoint lastEndPoint, PathSegment psRet)
        {
            ControlPoint cpRet = null;
            lastEndPoint = null;
            if (editMode)
            {
                ControlPoint lastCP = null;
                ControlPoint bezierTmpCP = null;

                PathGeom.DoForAllPointDPs(new DependencyPropertyCallback((DependencyObject obj, DependencyProperty dp) =>
                {
                    var cp = new ControlPoint(this, obj, dp, 0, true, false);

                    cp.Tag = obj;
                    shape_ControlPoints.Add(cp);
                    if (dp.OwnerType == typeof(BezierSegment))
                    {
                        if (dp.Name == "Point1") // control pointLabel 1
                        {
                            lastCP.SubPoint = cp;
                            cp.ConnectedTo = lastCP;
                            cp.IsSelectable = false;
                        }
                        else if (dp.Name == "Point2") // control pointLabel 2
                        {
                            cp.IsSelectable = false;
                            bezierTmpCP = cp;
                        }
                        else if (dp.Name == "Point3") // end pointLabel
                        {
                            cp.SubPoint = bezierTmpCP;
                            bezierTmpCP.ConnectedTo = cp;
                            lastCP = cp;
                        }
                    }
                    else if (dp.OwnerType == typeof(QuadraticBezierSegment))
                    {
                        if (dp.Name == "Point1") // control pointLabel 2
                        {
                            cp.IsSelectable = false;
                            bezierTmpCP = cp;
                        }
                        else if (dp.Name == "Point2") // end pointLabel
                        {
                            cp.SubPoint = bezierTmpCP;
                            bezierTmpCP.ConnectedTo = cp;
                            lastCP = cp;
                        }
                    }
                    else
                    {
                        lastCP = cp;
                    }
                    if (obj == psRet)
                    {
                        cpRet = lastCP;
                    }
                }));
                lastEndPoint = lastCP;
            }
            return cpRet;
        }

        #endregion   

   }
}
