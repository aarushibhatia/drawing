﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.ComponentModel;
using System.Xml.Linq;
using System.Globalization;
using System.Windows.Controls;
using Newtonsoft.Json;

namespace Drawing
{
    [XDrawGeometry("RectangleGeometry")]
    public class Vms_Rectangle : Vms_DrawingShape
    {
        #region DependencyProperties

        public static readonly DependencyProperty WidthProperty = DependencyProperty.Register(
         "Width", typeof(double), typeof(Vms_DrawingShape), new PropertyMetadata(1.0, WidthChanged));

        public static readonly DependencyProperty HeightProperty = DependencyProperty.Register(
           "Height", typeof(double), typeof(Vms_DrawingShape), new PropertyMetadata(1.0, HeightChanged));

        #endregion

        #region Variables

        private ControlPoint m_MoveControlPoint;
        private static string NameRect;
        private static string LabelRect = "0,0";

        #endregion

        #region Properties

        public RectangleGeometry RectGeom
        {
            get { return Path.Data as RectangleGeometry; }
        }

        public double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); }
        }

        public double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); }
        }

        #endregion

        #region Constructor

        public Vms_Rectangle(Path path)
   : base(path)
        {
            var shape = this;
            if (!(Path.Data is RectangleGeometry))
            {
                throw new ArgumentException("path.Data must be of type RectangleGeometry");
            }
            m_MoveControlPoint = new ControlPoint(this, RectGeom, RectangleGeometry.RectProperty, -1, false, false);
            Path.Tag = this;
            Width = RectGeom.Rect.Width;
            Height = RectGeom.Rect.Height;
            shape.Name = NameRect;
            shape.LabelPosition = Point.Parse(LabelRect);
            DependencyPropertyDescriptor.FromProperty(RectangleGeometry.RectProperty, typeof(RectangleGeometry)).AddValueChanged(Path.Data, RectChanged);
        }

        #endregion

        #region Internal Methods

        protected internal override Point GetPoint()
        {
            return m_MoveControlPoint.GetPoint();
        }

        protected override void SetPoint(Point pt)
        {
            m_MoveControlPoint.SetPoint(pt);
        }

        internal override void CreateControlPoints(bool editMode)
        {
            ControlPoint cp;
            CreateControlPoints(editMode, out cp);
        }

        #endregion

        #region Public Methods

        public void CreateControlPoints(bool editMode, out ControlPoint bottomRightControlPt)
        {
            shape_ControlPoints.Add(new ControlPoint(this, RectGeom, RectangleGeometry.RectProperty, 0, true, false));
            shape_ControlPoints.Add(new ControlPoint(this, RectGeom, RectangleGeometry.RectProperty, 1, true, false));
            shape_ControlPoints.Add(new ControlPoint(this, RectGeom, RectangleGeometry.RectProperty, 2, true, false));
            bottomRightControlPt = new ControlPoint(this, RectGeom, RectangleGeometry.RectProperty, 3, true, false);
            shape_ControlPoints.Add(bottomRightControlPt);
        }

        #endregion

        #region Events

        private void RectChanged(object sender, EventArgs e)
        {
            if (Width != RectGeom.Rect.Width)
            {
                Width = RectGeom.Rect.Width;
            }
            if (Height != RectGeom.Rect.Height)
            {
                Height = RectGeom.Rect.Height;
            }

        }

        private static void WidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var rect = (d as Vms_Rectangle).RectGeom.Rect;
            if (rect.Width != (double)e.NewValue)
            {
                rect.Width = (double)e.NewValue;
                (d as Vms_Rectangle).RectGeom.Rect = rect;
            }
        }

        private static void HeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var rect = (d as Vms_Rectangle).RectGeom.Rect;
            if (rect.Height != (double)e.NewValue)
            {
                rect.Height = (double)e.NewValue;
                (d as Vms_Rectangle).RectGeom.Rect = rect;
            }
        }

        #endregion

    }
}
