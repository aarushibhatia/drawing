﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Collections;
using System.Xml.Linq;
using System.ComponentModel;
using Microsoft.VisualBasic;

namespace Drawing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly DependencyProperty ToolbarButtonSizeProperty = DependencyProperty.Register(
       "ToolbarButtonSize", typeof(double), typeof(MainWindow), new PropertyMetadata(24.0));
        public MainWindow()
        {
            InitializeComponent();
        }

      

        public Vms_Drawing Drawing
        {
            get
            {
                if (tab.SelectedItem != null)
                {
                    return ((tab.SelectedItem as TabItem).DataContext as ProjectDrawing).Drawing;
                }
                return null;
            }
        }

        public double ToolbarButtonSize
        {
            get { return (double)GetValue(ToolbarButtonSizeProperty); }
            set { SetValue(ToolbarButtonSizeProperty, value); }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            // load window position and state
            var state = Properties.Settings.Default.MainWindow_State;
            if (!String.IsNullOrEmpty(state))
            {
                try
                {
                    WindowState = (WindowState)Enum.Parse(typeof(WindowState), state);
                }
                catch { }
            }
           
            var location = new Rect(
               Properties.Settings.Default.MainWindow_Left,
               Properties.Settings.Default.MainWindow_Top,
               Properties.Settings.Default.MainWindow_Width,
               Properties.Settings.Default.MainWindow_Height);

            if (location.Width > 0.0 && location.Height > 0.0)
            {
                Left = location.Left;
                Top = location.Top;
                Width = location.Width;
                Height = location.Height;
            }

            NewProject(true);

            var startupProjectFile = (Application.Current as App).StartupProjectFile;
            if (!String.IsNullOrEmpty(startupProjectFile) && File.Exists(startupProjectFile))
            {
                m_Project.Load(startupProjectFile);
            }
            CheckModeButton();
            if (m_Project != null)
            {

                var pDrawing = new ProjectDrawing();

                int n = 0;
                var name = "newDrawing";
                while (GetDrawing(name) != null)
                {
                    name = String.Format("newDrawing_{0}", ++n);
                }
                pDrawing.DrawingName = name;
                pDrawing.ExportFormat = m_Project.DefaultExportFormat;
                m_Project.Drawings.Add(pDrawing);
                projectTree.ExpandObject(m_Project);
                projectTree.SelectObject(pDrawing);
                OpenDrawing(pDrawing, m_Project);
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //if (m_Project != null)
            //{
            //    if (String.IsNullOrEmpty(m_Project.TextFilePath))
            //    {
            //        MessageBox.Show("XAML path must be set for project before saving!");
            //    }
            //    else
            //    {
            //        if (String.IsNullOrEmpty(m_Project.ProjectFilePath))
            //        {
            //            var sfd = new SaveFileDialog();
            //            sfd.DefaultExt = ".xdprj";
            //            sfd.Filter = "XDraw project files (*.xdprj)|*.xdprj|All files (*.*)|*.*";
            //            sfd.FilterIndex = 0;
            //            var result = sfd.ShowDialog(this);
            //            if (result.HasValue && result.Value)
            //            {
            //                m_Project.Save(sfd.FileName);
            //            }
            //        }
            //        else
            //        {
            //            m_Project.Save();
            //        }
            //    }
            //}
            // save Window position and state
            Properties.Settings.Default.MainWindow_State = WindowState.ToString();
            Properties.Settings.Default.MainWindow_Left = RestoreBounds.Left;
            Properties.Settings.Default.MainWindow_Top = RestoreBounds.Top;
            Properties.Settings.Default.MainWindow_Width = RestoreBounds.Width;
            Properties.Settings.Default.MainWindow_Height = RestoreBounds.Height;
        }

        private void DrawingModeChanged(object sender, EventArgs e)
        {
            CheckModeButton();
        }

        private void CheckModeButton()
        {
            if (Drawing != null)
            {
                //btnSelect.IsChecked = Drawing.Mode == XDrawingModes.Select;
              //  btnEdit.IsChecked = Drawing.Mode == XDrawingModes.NewPolygon;
                btnLine.IsChecked = Drawing.Mode == XDrawingModes.NewLine;
                btnCircle.IsChecked = Drawing.Mode == XDrawingModes.NewEllipse;
                btnRect.IsChecked = Drawing.Mode == XDrawingModes.NewRect;
                btnEllipse.IsChecked = Drawing.Mode == XDrawingModes.NewEllipse;
                btnPolygon.IsChecked = Drawing.Mode == XDrawingModes.NewPath;
                btnLineString.IsChecked = Drawing.Mode == XDrawingModes.NewPath;
                btnEditPolygon.IsChecked = Drawing.Mode == XDrawingModes.EditShapes;
              //  btnCircle.IsChecked = Drawing.Mode == XDrawingModes.NewCircle;
              
            }
        }

        /*private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
           if (Drawing != null)
           {
              Drawing.Mode = XDrawingModes.Select;
              CheckModeButton();
           }
        }*/

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                Drawing.Mode = XDrawingModes.NewPolygon;
                CheckModeButton();
            }
        }

        private void btnNewLine_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                Drawing.Mode = XDrawingModes.NewLine;
                CheckModeButton();
            }
        }

        private void btnNewRect_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                    Drawing.Mode = XDrawingModes.NewRect;
                CheckModeButton();
            }
        }

        private void btnNewEllipse_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                Vms_Drawing.isCircle = false;
                Drawing.Mode = XDrawingModes.NewEllipse;
                CheckModeButton();
            }
        }

        private void btnNewEditPolygon_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (btnEditPolygon.IsChecked == false)
                {
                    if(Drawing.SelectedShape != null)
                    {
                        Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                        Drawing.SelectedShape = null;
                    }  
                    Drawing.Mode = XDrawingModes.Name;
                    CheckModeButton();
                }
                else
                {
              
                    Drawing.Mode = XDrawingModes.EditShapes;
                    CheckModeButton();
                }
            }
        }


        private void btnNewPolygon_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {    
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                Drawing.isPolygon = true;
                Drawing.Mode = XDrawingModes.NewPath;
                CheckModeButton();
            }
        }

        private void btnNewText_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                Drawing.Mode = XDrawingModes.NewText;
                CheckModeButton();
            }
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "XDraw project files (*.xdprj)|*.xdprj|All files (*.*)|*.*";
            ofd.FilterIndex = 0;
            var result = ofd.ShowDialog(this);
            if (result.HasValue && result.Value)
            {
                NewProject(false);
                m_Project.Load(ofd.FileName);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (m_Project != null)
            {
                if (String.IsNullOrEmpty(m_Project.TextFilePath))
                {
                    MessageBox.Show("XAML path must be set for project before saving!");
                }
                else
                {
                    if (String.IsNullOrEmpty(m_Project.ProjectFilePath))
                    {
                        var sfd = new SaveFileDialog();
                        sfd.DefaultExt = ".xdprj";
                        sfd.Filter = "XDraw project files (*.xdprj)|*.xdprj|All files (*.*)|*.*";
                        sfd.FilterIndex = 0;
                        var result = sfd.ShowDialog(this);
                        if (result.HasValue && result.Value)
                        {
                            m_Project.Save(sfd.FileName);
                        }
                    }
                    else
                    {
                        m_Project.Save();
                    }
                }
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            NewProject(true);
        }

        private DrawProject m_Project = null;

        private void NewProject(bool showDialog)
        {
            CloseAllTabs();
           projectTree.Items.Clear();

            var prj = new DrawProject();
            prj.ProjectName = "New project";
            if (showDialog)
            {
                var dlg = new NewProjectDialog();
                dlg.Owner = this;
                dlg.Project = prj;
                var result = dlg.ShowDialog();
                if (!result.HasValue || !result.Value)
                {
                    return;
                }
            }
            m_Project = prj;
            projectTree.Items.Add(m_Project);
        }

        private void AddDrawing_Click(object sender, RoutedEventArgs e)
        {
            if (m_Project != null)
            {

                var pDrawing = new ProjectDrawing();

                int n = 0;
                var name = "newDrawing";
                while (GetDrawing(name) != null)
                {
                    name = String.Format("newDrawing_{0}", ++n);
                }
                pDrawing.DrawingName = name;
               pDrawing.ExportFormat = m_Project.DefaultExportFormat;
                m_Project.Drawings.Add(pDrawing);
                projectTree.ExpandObject(m_Project);
                projectTree.SelectObject(pDrawing);
                OpenDrawing(pDrawing, m_Project);
            }
        }

        private ProjectDrawing GetDrawing(string name)
        {
            if (m_Project != null)
            {
                foreach (var drw in m_Project.Drawings)
                {
                    if (String.Compare(name, drw.DrawingName) == 0)
                    {
                        return drw;
                    }
                }
            }
            return null;
        }


        private void PathSegmentTypeChange_Click(object sender, RoutedEventArgs e)
        {
            var shape = Drawing.SelectedShape as Vms_Polygon;
            if (shape != null && Drawing.SelectedControlPoint != null && Drawing.SelectedControlPoint.Tag is PathSegment)
            {
                Drawing.SelectedControlPoint = Drawing.ChangeSegmentType(Drawing.SelectedControlPoint, (string)(e.OriginalSource as RadioButton).Tag);
            }
        }

        private void miSmallButtons_Click(object sender, RoutedEventArgs e)
        {
            ToolbarButtonSize = 24.0;
            //miSmallButtons.IsChecked = true;
            //miLargeButtons.IsChecked = false;
        }

        private void miLargeButtons_Click(object sender, RoutedEventArgs e)
        {
            ToolbarButtonSize = 48.0;
            //miSmallButtons.IsChecked = false;
            //miLargeButtons.IsChecked = true;
        }

        //private void miAbout_Click(object sender, RoutedEventArgs e)
        //{
        //    var aboutDlg = new AboutDialog();
        //    aboutDlg.Owner = this;
        //    aboutDlg.ShowDialog();
        //}

        private void GridOptions_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                var dlg = new GridOptionsDialog();
                dlg.Owner = this;
                dlg.Drawing = Drawing;
                dlg.ShowDialog();
            }
        }

        private void projectTree_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var dObj = e.OriginalSource as DependencyObject;
                while (dObj != null && !(dObj is TreeViewItem))
                {
                    dObj = VisualTreeHelper.GetParent(dObj);
                }
                if (dObj != null)
                {
                    var pDrawing = (dObj as TreeViewItem).Header as ProjectDrawing;
                    if (pDrawing != null)
                    {
                        OpenDrawing(pDrawing, m_Project);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenDrawing(ProjectDrawing pDrawing, DrawProject m_Project)
        {
            TabItem ti = null;
            foreach (TabItem ti_ in tab.Items)
            {
                if (ti_.DataContext == pDrawing)
                {
                    ti = ti_;
                    break;
                }
            }

            if (ti == null)
            {
                ti = new TabItem();
                ti.DataContext = pDrawing;

                var sp = new StackPanel();
                sp.Orientation = Orientation.Horizontal;
                var tb = new TextBlock();
                tb.VerticalAlignment = VerticalAlignment.Center;
                BindingOperations.SetBinding(tb, TextBlock.TextProperty, new Binding("DrawingName"));
                sp.Children.Add(tb);
                var cb = new Button();
                var img = new Image();
                img.Source = FindResource("CloseImage") as ImageSource;
                cb.Content = img;
                cb.Click += CloseTab_Click;
                cb.Width = 16;
                cb.Height = 16;
                cb.Margin = new Thickness(5, 0, 0, 0);
                cb.Style = FindResource(ToolBar.ButtonStyleKey) as Style;
                sp.Children.Add(cb);
                ti.Header = sp;

                var scrollViewer = new ScrollViewer();
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.Background = new SolidColorBrush(Colors.Gray);

                var canvas = new Canvas();

                scrollViewer.Content = canvas;
                ti.Content = scrollViewer;

                var drawing = pDrawing.GetDrawing(canvas, m_Project);
                DependencyPropertyDescriptor.FromProperty(Vms_Drawing.ModeProperty, typeof(Vms_Drawing)).AddValueChanged(drawing, DrawingModeChanged);
                tab.Items.Add(ti);
            }
            tab.SelectedItem = ti;
         //   CheckModeButton();
        }

        void CloseTab_Click(object sender, RoutedEventArgs e)
        {
            var dObj = e.OriginalSource as DependencyObject;
            while (dObj != null && !(dObj is TabItem))
            {
                dObj = VisualTreeHelper.GetParent(dObj);
            }
            var ti = dObj as TabItem;
            if (ti != null)
            {
                CloseTab(ti);
            }
        }

        private void CloseTab(TabItem ti)
        {
            var pDrawing = ti.DataContext as ProjectDrawing;
            pDrawing.CloseDrawing();
            tab.Items.Remove(ti);
        }

        private void CloseAllTabs()
        {
            while (tab.Items.Count > 0)
            {
                CloseTab(tab.Items[tab.Items.Count - 1] as TabItem);
            }
        }

        private void ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                Drawing.ZoomIn();
            }
        }

        private void ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                Drawing.ZoomOut();
            }
        }

        private void Zoom1_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                Drawing.Zoom1();
            }
        }

        private void TreeViewItem_MouseRightButtonDown(object sender, MouseEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.Focus();
                e.Handled = true;
            }
        }

        //private void ExportDrawing_Click(object sender, RoutedEventArgs e)
        //{
        //    var pDrawing = projectTree.SelectedItem as XProjectDrawing;
        //    if (pDrawing != null)
        //    {
        //        var dlg = new ExportDrawingDialog();
        //        dlg.Owner = this;
        //        bool closeDrawing = false;
        //        if (pDrawing.Drawing != null)
        //        {
        //            dlg.Drawing = pDrawing.Drawing;
        //        }
        //        else
        //        {
        //            var canvas = new Canvas();
        //            dlg.Drawing = pDrawing.GetDrawing(canvas);
        //            closeDrawing = true;
        //        }
        //        dlg.ShowDialog();
        //        if (closeDrawing)
        //        {
        //            pDrawing.CloseDrawing();
        //        }
        //    }
        //}

        //private void ImportDrawing_Click(object sender, RoutedEventArgs e)
        //{
        //    var pDrawing = projectTree.SelectedItem as XProjectDrawing;
        //    if (pDrawing != null)
        //    {
        //        var dlg = new ImportDrawingDialog();
        //        dlg.Owner = this;
        //        bool closeDrawing = false;
        //        if (pDrawing.Drawing != null)
        //        {
        //            dlg.Drawing = pDrawing.Drawing;
        //        }
        //        else
        //        {
        //            var canvas = new Canvas();
        //            dlg.Drawing = pDrawing.GetDrawing(canvas);
        //            closeDrawing = true;
        //        }
        //        dlg.ShowDialog();
        //        if (closeDrawing)
        //        {
        //            pDrawing.CloseDrawing();
        //        }
        //    }
        //}

        //private void RemoveDrawing_Click(object sender, RoutedEventArgs e)
        //{
        //  //  var pDrawing = projectTree.SelectedItem as XProjectDrawing;
        //    if (pDrawing != null)
        //    {
        //        var result = MessageBox.Show(
        //           this,
        //           "Do you really want to delete the selected drawing?",
        //           "Delete drawing",
        //           MessageBoxButton.YesNo,
        //           MessageBoxImage.Question);
        //        if (result == MessageBoxResult.Yes)
        //        {
        //            foreach (TabItem ti_ in tab.Items)
        //            {
        //                if (ti_.DataContext == pDrawing)
        //                {
        //                    CloseTab(ti_);
        //                    break;
        //                }
        //            }
        //            m_Project.RemoveDrawing(pDrawing);
        //        }
        //    }
        //}

        //private void CopyDrawing_Click(object sender, RoutedEventArgs e)
        //{
        //    if (m_Project != null)
        //    {
        //        var pDrawing = projectTree.SelectedItem as XProjectDrawing;
        //        if (pDrawing != null)
        //        {
        //            var newDrawing = new XProjectDrawing();
        //            newDrawing.DrawingName = String.Format("copy of {0}", pDrawing.DrawingName);
        //            newDrawing.ExportFormat = pDrawing.ExportFormat;
        //            newDrawing.DrawingSize = pDrawing.DrawingSize;
        //            pDrawing.StoreXmlCode();
        //            newDrawing.XmlCode = new XElement(pDrawing.XmlCode);
        //            m_Project.Drawings.Add(newDrawing);
        //            projectTree.SelectObject(newDrawing);
        //            OpenDrawing(newDrawing, m_Project);
        //        }
        //    }
        //}

        private void tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
         //   CheckModeButton();
        }

        private void MainWindow_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            //textBox.Height = 15;
            //textBox.Width = 40;
            //textBox.Margin = new Thickness(e.GetPosition(this).Y - 30, e.GetPosition(this).X - 10, e.GetPosition(this).Y, e.GetPosition(this).X);
            //textBox.Visibility = Visibility.Visible;
        }

        private void ImportDrawing_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BoldText_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        (sender as CheckBox).IsChecked = (txt.FontWeight == FontWeights.Bold);
            //    }
            //}
        }

        private void ItalicText_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        (sender as CheckBox).IsChecked = (txt.FontStyle == FontStyles.Italic);
            //    }
            //}
        }

        private void ItalicText_Initialized(object sender, EventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        (sender as CheckBox).IsChecked = (txt.FontStyle == FontStyles.Italic);
            //    }
            //}
        }

        private void BoldText_Initialized(object sender, EventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        (sender as CheckBox).IsChecked = (txt.FontWeight == FontWeights.Bold);
            //    }
            //}
        }

        private void BoldText_Click(object sender, RoutedEventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        if ((e.OriginalSource as CheckBox).IsChecked == true)
            //        {
            //            txt.FontWeight = FontWeights.Bold;
            //        }
            //        else
            //        {
            //            txt.FontWeight = FontWeights.Normal;
            //        }
            //    }
            //}
        }

        private void ItalicText_Click(object sender, RoutedEventArgs e)
        {
            //if (Drawing != null)
            //{
            //    var txt = Drawing.SelectedShape as XDrawingText;
            //    if (txt != null)
            //    {
            //        if ((e.OriginalSource as CheckBox).IsChecked == true)
            //        {
            //            txt.FontStyle = FontStyles.Italic;
            //        }
            //        else
            //        {
            //            txt.FontStyle = FontStyles.Normal;
            //        }
            //    }
            //}
        }

        private void btnNewLineString_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {    
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                Drawing.isPolygon = false;
                Drawing.Mode = XDrawingModes.NewPath;
                CheckModeButton();
            }
        }

        private void btnNewCircle_Click(object sender, RoutedEventArgs e)
        {
            if (Drawing != null)
            {
                if (Drawing.SelectedShape != null)
                {
                    Drawing.SelectedShape.Path.Stroke = new SolidColorBrush(Colors.Black);
                    Drawing.SelectedShape = null;
                }
                Vms_Drawing.isCircle = true;
                Drawing.Mode = XDrawingModes.NewEllipse;
                CheckModeButton();
            }
        }
    }
}
