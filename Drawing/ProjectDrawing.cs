﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Globalization;
using Microsoft.Win32;
using System.IO;
using Microsoft.VisualBasic;

namespace Drawing
{
    public class ProjectDrawing : DependencyObject
    {
        #region DependencyProperties

        public static readonly DependencyProperty DrawingNameProperty =
       DependencyProperty.Register("DrawingName", typeof(string), typeof(ProjectDrawing), new UIPropertyMetadata(String.Empty));

        public static readonly DependencyProperty DrawingSizeProperty =
            DependencyProperty.Register("DrawingSize", typeof(Size), typeof(ProjectDrawing), new UIPropertyMetadata(new Size(1000.0, 1000.0)));

        public static readonly DependencyProperty ExportFormatProperty =
            DependencyProperty.Register("ExportFormat", typeof(XDrawingExportFormat), typeof(ProjectDrawing), new UIPropertyMetadata(XDrawingExportFormat.Canvas));

        public static readonly DependencyProperty SaveDrawingSizeProperty =
        DependencyProperty.Register("SaveDrawingSize", typeof(bool), typeof(ProjectDrawing), new UIPropertyMetadata(false));

        #endregion

        #region Private Variables

        private XElement m_XmlCode = null;
        private Vms_Drawing m_Drawing = null;

        #endregion

        #region Properties

        public string DrawingName
        {
            get { return (string)GetValue(DrawingNameProperty); }
            set { SetValue(DrawingNameProperty, value); }
        }

        public Size DrawingSize
        {
            get { return (Size)GetValue(DrawingSizeProperty); }
            set { SetValue(DrawingSizeProperty, value); }
        }

        public XDrawingExportFormat ExportFormat
        {
            get { return (XDrawingExportFormat)GetValue(ExportFormatProperty); }
            set { SetValue(ExportFormatProperty, value); }
        }

        public bool SaveDrawingSize
        {
            get { return (bool)GetValue(SaveDrawingSizeProperty); }
            set { SetValue(SaveDrawingSizeProperty, value); }
        }

        public XElement XmlCode
        {
            get
            {
                // get lastest xml code
                StoreXmlCode();
                return m_XmlCode;
            }
            set { m_XmlCode = value; }
        }

        #endregion

        #region Constructor

        public ProjectDrawing() { }

        #endregion

        #region Methods

        /// <summary>
        /// Saves all drawing groups in a project.
        /// </summary>
        /// <param name="xParent">
        /// A project which contains different drawing groups.
        /// </param>
        internal void Save(XElement xParent)
        {
            var nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";

            xParent.Add(
                new XElement("Drawing",
                    new XAttribute("Name", DrawingName),
                    new XElement("ExportFormat", ExportFormat.ToString()),
                    new XElement("DrawingSize", DrawingSize.ToString(nfi)),
                    new XElement("SaveDrawingSize", SaveDrawingSize.ToString())));
        }

        /// <summary>
        /// Returns Drawing Group.
        /// </summary>
        public Vms_Drawing Drawing
        {
            get { return m_Drawing; }
        }

        /// <summary>
        /// Returns Drawing Group.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="m_Project"> Project which contains drawing group to be returned. </param>
        /// <returns></returns>
        public Vms_Drawing GetDrawing(Canvas canvas, DrawProject m_Project)
        {
            if (m_Drawing != null && m_Drawing.Canvas != canvas)
            {
                m_Drawing.Canvas.ClearValue(Canvas.WidthProperty);
                m_Drawing.Canvas.ClearValue(Canvas.HeightProperty);
                m_Drawing.Dispose();
                m_Drawing = null;
            }
            if (m_Drawing == null)
            {        
                var bw = new Binding("DrawingSize.Width");
                bw.Source = this;
                canvas.SetBinding(Canvas.WidthProperty, bw);
                bw = new Binding("DrawingSize.Height");
                bw.Source = this;
                canvas.SetBinding(Canvas.HeightProperty, bw);

                m_Drawing = new Vms_Drawing(canvas);

                if (new FileInfo(m_Project.TextFilePath).Exists)
                {
                    if (new FileInfo(m_Project.TextFilePath).Length != 0)
                    {
                        MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to import existing project?", "File already exists.", MessageBoxButton.YesNo);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            m_Drawing.Import(m_Project);
                        }
                        else
                        {
                            m_Project.TextFilePath = Interaction.InputBox("This file exists. Enter another name with '.docx' extension.", "", "");
                            FileStream fs = File.Create(m_Project.TextFilePath);
                        }
                    }  
                }                             
           }
            return m_Drawing;
        }

        public void CloseDrawing()
        {
            if (m_Drawing != null)
            {
                m_Drawing.Dispose();
                m_Drawing = null;
            }
        }

        public void StoreXmlCode()
        {
            if (m_Drawing != null)
            {
                m_XmlCode = m_Drawing.Export(ExportFormat);
            }
        }

        #endregion
    }
}
























