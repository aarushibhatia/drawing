﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Reflection;
using System.Windows.Media;

namespace Drawing
{
    #region Class PathGeometryExtensions

    public static class PathGeometryExtensions
    {
        #region Methods

        public static Point GetFirstStartPoint(this PathGeometry path)
        {
            if (path.Figures.Count > 0)
            {
                return path.Figures[0].StartPoint;
            }
            return new Point();
        }

        public static void DoForAllPoints(this PathGeometry path, RefPointCallback callback)
        {
            foreach (var f in path.Figures)
            {
                var pt = f.StartPoint;
                callback(ref pt);
                f.StartPoint = pt;
                foreach (var s in f.Segments)
                {
                    foreach (FieldInfo fi in s.GetType().GetFields())
                    {
                        if (fi.FieldType == typeof(DependencyProperty))
                        {
                            var dp = fi.GetValue(null) as DependencyProperty;
                            if (dp.PropertyType == typeof(Point))
                            {
                                pt = (Point)s.GetValue(dp);
                                callback(ref pt);
                                s.SetValue(dp, pt);
                            }
                        }
                    }
                }
            }
        }

        public static void DoForAllPointDPs(this PathGeometry path, DependencyPropertyCallback callback)
        {
            foreach (var f in path.Figures)
            {
                var t1 = f.GetType();
                var fii = t1.GetField("StartPointProperty");
                var oo = fii.GetValue(null);
                callback(f, oo as DependencyProperty);
                foreach (var s in f.Segments)
                {
                    foreach (FieldInfo fi in s.GetType().GetFields())
                    {
                        if (fi.FieldType == typeof(DependencyProperty))
                        {
                            var dp = fi.GetValue(null) as DependencyProperty;
                            if (dp.PropertyType == typeof(Point))
                            {
                                callback(s, dp);
                            }
                            else
                            {
                                MessageBox.Show("Not pointLabel");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Not pointLabel");
                        }
                    }
                }
            }
        }

        #endregion
    }

    #endregion

    #region Class PathSegmentExtensions

    public static class PathSegmentExtensions
    {
        #region Methods

        public static void DoForAllPoints(this PathSegment segment, RefPointCallback callback)
        {
            foreach (FieldInfo fi in segment.GetType().GetFields())
            {
                if (fi.FieldType == typeof(DependencyProperty))
                {
                    var dp = fi.GetValue(null) as DependencyProperty;
                    if (dp.PropertyType == typeof(Point))
                    {
                        var pt = (Point)segment.GetValue(dp);
                        callback(ref pt);
                        segment.SetValue(dp, pt);
                    }
                }
            }
        }

        public static void DoForAllPointDPs(this PathSegment segment, DependencyPropertyCallback callback)
        {
            foreach (FieldInfo fi in segment.GetType().GetFields())
            {
                if (fi.FieldType == typeof(DependencyProperty))
                {
                    var dp = fi.GetValue(null) as DependencyProperty;
                    if (dp.PropertyType == typeof(Point))
                    {
                        callback(segment, dp);
                    }
                }
            }
        }

    #endregion
    }

    #endregion

    public delegate void RefPointCallback(ref Point pt);

    public delegate void DependencyPropertyCallback(DependencyObject obj, DependencyProperty dp);
}



