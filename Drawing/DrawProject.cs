﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using Newtonsoft.Json;
using System.Windows.Media;

namespace Drawing
{
   public class DrawProject : DependencyObject
   {
        #region DependencyProperties

        public static readonly DependencyProperty DefaultExportFormatProperty =
       DependencyProperty.Register("DefaultExportFormat", typeof(XDrawingExportFormat), typeof(DrawProject), new UIPropertyMetadata(XDrawingExportFormat.DrawingImage));

        public static readonly DependencyProperty ProjectFilePathProperty =
      DependencyProperty.Register("ProjectFilePath", typeof(string), typeof(DrawProject), new UIPropertyMetadata(String.Empty));

        public static readonly DependencyProperty ProjectNameProperty =
          DependencyProperty.Register("ProjectName", typeof(string), typeof(DrawProject), new UIPropertyMetadata(String.Empty));

        public static readonly DependencyProperty XAMLFilePathProperty =
        DependencyProperty.Register("TextFilePath", typeof(string), typeof(DrawProject), new UIPropertyMetadata(String.Empty));

        #endregion

        #region Properties

        public ObservableCollection<ProjectDrawing> Drawings { get; private set; }

        public XDrawingExportFormat DefaultExportFormat
        {
            get { return (XDrawingExportFormat)GetValue(DefaultExportFormatProperty); }
            set { SetValue(DefaultExportFormatProperty, value); }
        }

        public string ProjectFilePath
        {
            get { return (string)GetValue(ProjectFilePathProperty); }
            set { SetValue(ProjectFilePathProperty, value); }
        }

        public string ProjectName
        {
            get { return (string)GetValue(ProjectNameProperty); }
            set { SetValue(ProjectNameProperty, value); }
        }

        public string TextFilePath
        {
            get { return (string)GetValue(XAMLFilePathProperty); }
            set { SetValue(XAMLFilePathProperty, value); }
        }

        #endregion

        #region Constructor

        public DrawProject()
        {
            Drawings = new ObservableCollection<ProjectDrawing>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Imports an already existing project
        /// </summary>
        /// <param name="path"></param>
        public void Load(string path)
        {
            ProjectFilePath = path;

            var doc = XDocument.Load(ProjectFilePath);
            var xPrj = new
            {
                Version = (string)doc.Root.Elements("FileVersion").FirstOrDefault(),
                ProjectName = (string)doc.Root.Elements("ProjectName").FirstOrDefault(),
                DefaultExportFormat = (string)doc.Root.Elements("DefaultExportFormat").FirstOrDefault(),
                XAMLFilePath = (string)doc.Root.Elements("TextFilePath").FirstOrDefault(),
                Drawings = from d in doc.Root.Descendants("Drawing")
                           select new
                           {
                               DrawingName = (string)d.Attributes("Name").FirstOrDefault(),
                               ExportFormat = (string)d.Elements("ExportFormat").FirstOrDefault(),
                               DrawingSize = (string)d.Elements("DrawingSize").FirstOrDefault(),
                               SaveDrawingSize = (string)d.Elements("SaveDrawingSize").FirstOrDefault()
                           }
            };

            ProjectName = xPrj.ProjectName ?? String.Empty;
            if (xPrj.DefaultExportFormat != null)
            {
                DefaultExportFormat = (XDrawingExportFormat)Enum.Parse(typeof(XDrawingExportFormat), xPrj.DefaultExportFormat);
            }
            TextFilePath = xPrj.XAMLFilePath ?? String.Empty;

            foreach (var d in xPrj.Drawings)
            {
                var drawing = new ProjectDrawing();
                drawing.DrawingName = d.DrawingName ?? String.Empty;
                if (d.ExportFormat != null)
                {
                    drawing.ExportFormat = (XDrawingExportFormat)Enum.Parse(typeof(XDrawingExportFormat), d.ExportFormat);
                }
                else
                {
                    drawing.ExportFormat = DefaultExportFormat;
                }
                if (d.DrawingSize != null)
                {
                    drawing.DrawingSize = Size.Parse(d.DrawingSize);
                }
                if (d.SaveDrawingSize != null)
                {
                    drawing.SaveDrawingSize = Boolean.Parse(d.SaveDrawingSize);
                }
                Drawings.Add(drawing);
            }

            var xamlPath = TextFilePath;
            if (!Path.IsPathRooted(xamlPath))
            {
                xamlPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(ProjectFilePath), xamlPath));
            }
            if (File.Exists(xamlPath))
            {
                var xamlDoc = XDocument.Load(xamlPath);

                XNamespace nsx = "http://schemas.microsoft.com/winfx/2006/xaml";

                var xDrawings = from d in xamlDoc.Root.Elements()
                                where d.Name.LocalName == "Canvas" || d.Name.LocalName == "DrawingImage"
                                select new
                                {
                                    XmlCode = d,
                                    Key = d.Attribute(nsx + "Key").Value
                                };

                foreach (var d in xDrawings)
                {
                    var drawing = GetDrawing(d.Key);
                    if (drawing == null)
                    {
                        drawing = new ProjectDrawing();
                        drawing.DrawingName = d.Key;
                       drawing.ExportFormat = d.XmlCode.Name.LocalName == "Canvas" ? XDrawingExportFormat.Canvas : XDrawingExportFormat.DrawingImage;
                        Drawings.Add(drawing);
                    }
                    drawing.XmlCode = new XElement(d.XmlCode);
                    foreach (var xe in drawing.XmlCode.DescendantsAndSelf())
                    {
                        xe.Name = xe.Name.LocalName;
                    }
                    drawing.XmlCode.RemoveAttributes();
                }
            }
        }

        /// <summary>
        /// Gets DrawingGroup of Project
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ProjectDrawing GetDrawing(string name)
        {
            foreach (var d in Drawings)
            {
                if (d.DrawingName == name)
                {
                    return d;
                }
            }
            return null;
        }

        /// <summary>
        /// Saves Project and DrawingGroup.
        /// </summary>
        public void Save()
        {
            if (String.IsNullOrEmpty(ProjectFilePath))
            {
                throw new Exception("ProjectFilePath must be valid to call Save(). Use Save(path) instead!");
            }
            if (String.IsNullOrEmpty(TextFilePath))
            {
                throw new Exception("TextFilePath must be valid to Save!");
            }

            var xDrawings = new XElement("Drawings");
            foreach (var d in Drawings)
            {
                d.Save(xDrawings);
            }
            var doc = new XDocument(
               new XElement("XDrawProject",
                  new XElement("FileVersion", "1.0"),
                  new XElement("ProjectName", ProjectName),
                  new XElement("DefaultExportFormat", DefaultExportFormat.ToString()),
                  new XElement("TextFilePath", TextFilePath),
                  xDrawings));

            doc.Save(ProjectFilePath);

            CreateTextFile();
        }

        public void Save(string path)
        {
            ProjectFilePath = path;
            Save();
        }

        /// <summary>
        /// Creates Text File of information about shapes contained in Canvas.
        /// Stores in string format of each json type shape.
        /// </summary>
        private void CreateTextFile()
        {
            StreamWriter document = new StreamWriter(TextFilePath, true);

            if (!File.Exists(TextFilePath))
            {
                document = new StreamWriter(TextFilePath);
            }
            foreach (var shape in Vms_Drawing.m_Shapes)
            {
                var type = shape.ShapeType;
                string shapeDetails = null;
                switch (type)
                {
                    case "Vms_Ellipse":
                        var ellipse = shape as Vms_Ellipse;
                        var data = new Ellipse()
                        { 
                            Name = shape.Name,
                            LabelPosition = shape.LabelPosition,
                            Type = shape.ShapeType,
                            RadiusX = ellipse.EllipseGeom.RadiusX,
                            RadiusY = ellipse.EllipseGeom.RadiusY,
                            Center = ellipse.EllipseGeom.Center
                        };
                        shapeDetails = JsonConvert.SerializeObject(data);
                        break;

                    case "Vms_Line":
                        var line = shape as Vms_Line;
                        var data2 = new Line()
                        {
                            Name = shape.Name,
                            LabelPosition = shape.LabelPosition,
                            Type = shape.ShapeType,
                            StartPoint = line.LineGeom.StartPoint,
                            EndPoint = line.LineGeom.EndPoint
                           
                        };
                        shapeDetails = JsonConvert.SerializeObject(data2);
                        break;

                    case "Vms_Rectangle":
                        var rect = shape as Vms_Rectangle;
                        var data3 = new Rectangle()
                        {
                            Name = shape.Name,
                            LabelPosition = shape.LabelPosition,
                            Type = shape.ShapeType,
                            RadiusX = rect.RectGeom.RadiusX,
                            RadiusY = rect.RectGeom.RadiusY,
                           Rect = rect.RectGeom.Rect
                        };
                        shapeDetails = JsonConvert.SerializeObject(data3);
                        break;

                    case "Vms_Polygon":
                        var polygon = shape as Vms_Polygon;     
                        var data4 = new Polygon()
                        {
                            Name = shape.Name,
                            LabelPosition = shape.LabelPosition,
                            Type = shape.ShapeType,
                            pathGeo = polygon.Path.Data
                        };
                        shapeDetails = JsonConvert.SerializeObject(data4);
                        break;
                }
                document.WriteLine(shapeDetails);
            }
            document.Close();
        }
        
        #endregion

    }
}
